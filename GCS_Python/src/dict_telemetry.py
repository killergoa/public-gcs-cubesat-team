'''
Created on 22/mag/2014

@author: Lorenzo Feruglio
'''
#importo TelemtryBox solo xk non dia errore sotto, che non sa che cosa e' una telemetry box
#from GUI import TelemetryBox
from dict_conversions import *
def create_telemetry_dict():
    
    #creo un dizionario con le tre categorie, come da immagine
    telemetry_dict = {'STATUS':dict(),'EPS':dict(),'ADCS':dict()}
    #qui puoi iniziare a riempire le varie categorie
    #per il tipo, ci potrebbe servire dopo. Metti uno tra i seguenti, oppure
    #altri se ti servono: int, string, float
        
    '''PARAMETRI STATUS'''
    telemetry_dict['STATUS'][1] = {'value':[],'name':'Packet Number', 'type':'int','object':None, 'conversion_dict': packetn_dict}
    telemetry_dict['STATUS'][2] = {'value':[],'name':'Release Time','type':'string','object':None, 'conversion_dict': releasetime_dict}
    telemetry_dict['STATUS'][3] = {'value':[],'name':'Ground Time','type':'int','object':None, 'conversion_dict': releasetime_dict}
    telemetry_dict['STATUS'][4] = {'value':[],'name':'Mission Time','type':'int','object':None, 'conversion_dict': satellitetime_dict}
    telemetry_dict['STATUS'][5] = {'value':[],'name':'N Reboot','type':'int','object':None, 'conversion_dict': nrebootobc_dict}
    telemetry_dict['STATUS'][6] = {'value':[],'name':'n Reboot ARM','type':'int','object':None, 'conversion_dict':nRebootARM_dict}
    telemetry_dict['STATUS'][7] = {'value':'Basic mission','name':'Operative Mode','type':'string','object':None, 'conversion_dict': operativemode_dict}
    #telemetry_dict['STATUS'][6] = {'value':[],'name':'EPS 1','type':'string','object':None, 'conversion_dict': statuseps_dict}
    #telemetry_dict['STATUS'][7] = {'value':[],'name':'EPS 2','type':'string','object':None, 'conversion_dict': statuseps_dict}
    telemetry_dict['STATUS'][8] = {'value':[],'name':'ADCS','type':'string','object':None, 'conversion_dict': statusadcs_dict}
    telemetry_dict['STATUS'][9] = {'value':[],'name':'last Command','type':'string','object':None, 'conversion_dict': lastcommand_dict}
    telemetry_dict['STATUS'][10] = {'value':[],'name':'Signature','type':'string','object':None, 'conversion_dict': signature_dict}
    '''PARAMETRI EPS'''
    telemetry_dict['EPS'][1] = {'value':[],'name':'5V Cur[mA]','type':'float','object':None  , 'conversion_dict': cur5v_dict}
    telemetry_dict['EPS'][2] = {'value':[],'name':'3.3V Cur[mA]','type':'float','object':None, 'conversion_dict': cur33v_dict}
    telemetry_dict['EPS'][3] = {'value':[],'name':'BAT Cur[mA]','type':'float','object':None , 'conversion_dict': batbuscur_dict}
                    
    telemetry_dict['EPS'][4] = {'value':[],'name':'BAT 1-Status','type':'string','object':None , 'conversion_dict':bat1status_dict}
    telemetry_dict['EPS'][5] = {'value':[],'name':'BAT 1-Vol [V]','type':'float','object':None , 'conversion_dict':bat1vol_dict}   
    telemetry_dict['EPS'][6] = {'value':[],'name':'BAT 1-Cur [mA]','type':'float','object':None, 'conversion_dict':bat1cur_dict}  
    telemetry_dict['EPS'][7] = {'value':[],'name':'BAT 1-Temp [C]','type':'float','object':None, 'conversion_dict':bat1temp_dict}
                    
    telemetry_dict['EPS'][8] = {'value':[],'name':'BAT 2-Status','type':'string','object':None  , 'conversion_dict':bat2status_dict}
    telemetry_dict['EPS'][9] = {'value':[],'name':'BAT 2-Vol [V]','type':'float','object':None  , 'conversion_dict':bat2vol_dict}
    telemetry_dict['EPS'][10] = {'value':[],'name':'BAT 2-Cur [mA]','type':'float','object':None, 'conversion_dict':bat2cur_dict}
    telemetry_dict['EPS'][11] = {'value':[],'name':'BAT 2-Temp [C]','type':'float','object':None, 'conversion_dict':bat2temp_dict}
    
    telemetry_dict['EPS'][12] = {'value':[],'name':'SP X Vol[V]','type':'float','object':None, 'conversion_dict':spxvol_dict}
    telemetry_dict['EPS'][13] = {'value':[],'name':'SP Y Vol[V]','type':'float','object':None, 'conversion_dict':spyvol_dict}
    telemetry_dict['EPS'][14] = {'value':[],'name':'SP Z Vol[V]','type':'float','object':None, 'conversion_dict':spzvol_dict}
                    
    telemetry_dict['EPS'][15] = {'value':[],'name':'SP +X Cur[mA]','type':'float','object':None, 'conversion_dict':spplusxcur_dict}
    telemetry_dict['EPS'][16] = {'value':[],'name':'SP +Y Cur[mA]','type':'float','object':None, 'conversion_dict':spplusycur_dict}
    telemetry_dict['EPS'][17] = {'value':[],'name':'SP -Y Cur[mA]','type':'float','object':None, 'conversion_dict':spminycur_dict} 
    telemetry_dict['EPS'][18] = {'value':[],'name':'SP +Z Cur[mA]','type':'float','object':None, 'conversion_dict':sppluszcur_dict}
    telemetry_dict['EPS'][19] = {'value':[],'name':'SP -Z Cur[mA]','type':'float','object':None, 'conversion_dict':spminzcur_dict} 
                    
    telemetry_dict['EPS'][20] = {'value':[],'name':'SP +X Temp[C]','type':'float','object':None, 'conversion_dict':spplusxtemp_dict}
    telemetry_dict['EPS'][21] = {'value':[],'name':'SP +Y Temp[C]','type':'float','object':None, 'conversion_dict':spplusytemp_dict}
    telemetry_dict['EPS'][22] = {'value':[],'name':'SP -Y Temp[C]','type':'float','object':None, 'conversion_dict':spminytemp_dict}
    telemetry_dict['EPS'][23] = {'value':[],'name':'SP +Z Temp[C]','type':'float','object':None, 'conversion_dict':spplusztemp_dict}
    telemetry_dict['EPS'][24] = {'value':[],'name':'SP -Z Temp[C]','type':'float','object':None, 'conversion_dict':spminztemp_dict}
    #PARAMETRI ADCS
    
    
    telemetry_dict['ADCS'][1] = {'value':[],'name':'q1','type':'string','object':None, 'NA_flag':True,'conversion_dict':q1_dict}
    telemetry_dict['ADCS'][2] = {'value':[],'name':'q2','type':'string','object':None, 'NA_flag':True, 'conversion_dict':q2_dict}
    telemetry_dict['ADCS'][3] = {'value':[],'name':'q3','type':'string','object':None, 'NA_flag':True, 'conversion_dict':q3_dict}
    telemetry_dict['ADCS'][4] = {'value':[],'name':'q4','type':'string','object':None, 'NA_flag':True, 'conversion_dict':q4_dict}
    
    telemetry_dict['ADCS'][5] = {'value':[],'name':'GyrX[rad/s]','type':'float','object':None, 'NA_flag':True, 'conversion_dict':gyrX_dict}
    telemetry_dict['ADCS'][6] = {'value':[],'name':'GyrY[rad/s]','type':'float','object':None, 'NA_flag':True,'conversion_dict':gyrY_dict}
    telemetry_dict['ADCS'][7] = {'value':[],'name':'GyrZ[rad/s]','type':'float','object':None, 'NA_flag':True,'conversion_dict':gyrZ_dict}

    telemetry_dict['ADCS'][8] = {'value':[],'name':'Mean Bdot Norm 0','type':'float','object':None, 'NA_flag':True, 'conversion_dict':MeanBdotNorm0_dict}
    telemetry_dict['ADCS'][9] = {'value':[],'name':'Mean Bdot Norm 1','type':'float','object':None, 'NA_flag':True, 'conversion_dict':MeanBdotNorm1_dict}
    telemetry_dict['ADCS'][10] = {'value':[],'name':'Mean Bdot Norm 2','type':'float','object':None, 'NA_flag':True, 'conversion_dict':MeanBdotNorm2_dict}
    
    telemetry_dict['ADCS'][11] = {'value':[],'name':'MagX[mG]','type':'float','object':None, 'NA_flag':True, 'conversion_dict':magX_dict}
    telemetry_dict['ADCS'][12] = {'value':[],'name':'MagY[mG]','type':'float','object':None, 'NA_flag':True, 'conversion_dict':magY_dict}
    telemetry_dict['ADCS'][13] = {'value':[],'name':'MagZ[mG]','type':'float','object':None, 'NA_flag':True, 'conversion_dict':magZ_dict}
    
    telemetry_dict['ADCS'][14] = {'value':[],'name':'CoilX[mA]','type':'float','object':None, 'NA_flag':True, 'conversion_dict':coilX_dict}
    telemetry_dict['ADCS'][15] = {'value':[],'name':'CoilY[mA]','type':'float','object':None, 'NA_flag':True, 'conversion_dict':coilY_dict}
    telemetry_dict['ADCS'][16] = {'value':[],'name':'CoilZ[mA]','type':'float','object':None, 'NA_flag':True, 'conversion_dict':coilZ_dict}
    telemetry_dict['ADCS'][17] = {'value':[],'name':'S flag','type':'int','object':None, 'NA_flag':True, 'conversion_dict':sflag_dict}
    telemetry_dict['ADCS'][18] = {'value':[],'name':'Polarity','type':'int','object':None, 'NA_flag':True, 'conversion_dict':polarity_dict}
    telemetry_dict['ADCS'][19] = {'value':[],'name':'Ist. FIR output [*10E-05]','type':'float', 'NA_flag':True,'object':None, 'conversion_dict':IstFIRout_dict}
    telemetry_dict['ADCS'][20] = {'value':[],'name':'Acc Norm','type':'float','object':None, 'NA_flag':True, 'conversion_dict':accNorm_dict}
    
    telemetry_list=['STATUS', 'EPS', 'ADCS']
                
    return telemetry_dict, telemetry_list
    
    