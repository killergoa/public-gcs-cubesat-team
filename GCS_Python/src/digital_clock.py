'''
Created on 19/giu/2014

@author: Giovanna Rinaldi
'''
#!/usr/bin/python

# analogclock.py


    # use the wxPython LEDNumberCtrl widget for nice LED clock
    # Python25 and wxPython28 by HAB 07/30/2007
import time
import wx
import wx.gizmos as gizmos
class LED_clock(wx.Frame):
    """
    create nice LED clock showing the current time
    """
    def __init__(self, parent, id, pos):
        wx.Frame.__init__(self, parent, id, title='LED Clock', pos=pos, size=(350, 100))
        size = wx.DefaultSize
        style = gizmos.LED_ALIGN_CENTER
        self.led = gizmos.LEDNumberCtrl(self, -1, pos, size, style)
        # default colours are green on black
        self.led.SetBackgroundColour("black")
        self.led.SetForegroundColour("yellow")
        self.OnTimer(None)
        self.timer = wx.Timer(self, -1)
        # update clock digits every second (1000ms)
        self.timer.Start(1000)
        self.Bind(wx.EVT_TIMER, self.OnTimer)
        #self.Centre()
    def OnTimer(self, event):
        # get current time from computer
        current = time.localtime(time.time())
        # time string can have characters 0..9, -, period, or space
        ts = time.strftime("%H %M %S", current)
        self.led.SetValue(ts)
        
def start(pos):
    '''
    This external function (not a method of any class) has been created in order to allow
    execution of this file as a standalone app (with the if check on name) or to call it from
    GUI program, on button press
    '''
    app = wx.App()
    frame = LED_clock(None, -1, pos)
    frame.Show(True)
    app.SetTopWindow(frame)
    app.MainLoop()
    
# test the clock ...
if __name__ == '__main__':
    start()
    
