'''
Created on 01/set/2014

@author: ws-linux
'''

#!/usr/bin/python

# menu2.py
import os
import wx
import GUI_telemetry
import time
from resources import resource_path

class MyMenu(wx.Frame):
    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title, wx.DefaultPosition, wx.Size(380, 250))
        favicon = wx.Icon(resource_path('cube.ico','img'),
                           wx.BITMAP_TYPE_ICO)
        self.SetIcon(favicon)        
        self.folder_path=None
        self.folder_name=None
        self.menubar = wx.MenuBar()
        file = wx.Menu()
        edit = wx.Menu()
        help = wx.Menu()
        file.Append(101, '&New', 'Creates a new session')
        file.Append(102, '&Open Complete', 'Open a past session for continuing')
        file.Append(103, '&Open Peer', 'Open the GUI in peer mode')
        file.AppendSeparator()
        quit = wx.MenuItem(file, 105, '&Quit\tCtrl+Q', 'Quit the Application')
        quit.SetBitmap(wx.Image(resource_path('stock_exit-16.png','ico'),wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        file.AppendItem(quit)
        self.menubar.Append(file, '&File')
        self.SetMenuBar(self.menubar)
        self.Centre()
        self.Bind(wx.EVT_MENU, self.OnQuit, id=105)
        self.Bind(wx.EVT_MENU, self.OnNew, id=101)
        self.Bind(wx.EVT_MENU, self.OnOpen, id=102)
        self.Bind(wx.EVT_MENU, self.OnOpenPeer, id=103)
        self.Bind(wx.EVT_CLOSE, self.OnQuit)
        
    def opendir(self, event):
        dlg = wx.DirDialog(self, "Choose a directory:", style=wx.DD_DEFAULT_STYLE | wx.DD_NEW_DIR_BUTTON)
        if dlg.ShowModal() == wx.ID_OK:
            self.folder_path = dlg.GetPath()
        if self.folder_path == None:
            return None
        return 'ok'
        
    def OnNew(self, event):
        while True:
            self.opendir(event)
            if self.folder_path!=None:
                break
            else:
                dlg=wx.MessageDialog(None, 'No path selected, do you want to quit?',
                         'Quit?', wx.YES_NO) 
                result=dlg.ShowModal()
                dlg.Destroy()
                if result == wx.ID_YES:
                    self.OnQuit(event)
                    return False
        self.run_GUI('w','Complete')

    def OnOpen(self, event):
        if self.opendir(event) != None:
            self.run_GUI('a', 'Complete')
        else:
            print 'Canceled.'
    
    def OnOpenPeer(self, event):
        self.run_GUI('a', 'Peer')
    
    def run_GUI(self, rwa_option, mode):
        #self.menubar.Destroy()
        self.Hide()
        GUI_telemetry.start(self.folder_path, rwa_option, mode, self)
        
    def signal_stop(self):
        self.Destroy()

    def OnQuit(self, event=None):
        self.Destroy()

def start():
    '''
    This external function (not a method of any class) has been created in order to allow
    execution of this file as a standalone app (with the if check on name) or to call it from
    GUI program, on button press
    '''

    frame=MyMenu(None, -1, 'GCS e-st@r-II CubeSat Team PoliTo',)
    frame.Show(True)
    #wx.lib.inspection.InspectionTool().Show()

if __name__ == '__main__':
    print 'Cannot run this program as main! Run GUI_main.py'
#     app = wx.App(redirect=False)
#     start()  
#     if __name__ == '__main__':
#         app.MainLoop()  
