'''
Created on 26 gen 2016

@author: Lorenzo Feruglio
credits: PyEphem, brainwagon.org
'''
import sys
import math
import ephem
import wx



def next_passes(number, verbose):
    '''
    This function prints the next passages
    '''
    passes_list=[]
    fp=open('../data/tle.txt','r')
    lines=fp.readlines()
    fp.close()
    
    iss = ephem.readtle(lines[0].strip(),
                        lines[1].strip(),
                        lines[2].strip())
    
    obs = ephem.Observer()
    obs.lat = '44.699260'
    obs.long = '7.836740'
    for p in range(number):
        tr, azr, tt, altt, ts, azs = obs.next_pass(iss)
        passes_list+=[{'start':tr,'end':ts}]
        if verbose:
            print """Date/Time (UTC)       Alt/Azim      Lat/Long    Elev"""
            print """====================================================="""
            print tr
            print ts
        passes_list[-1]['alt']       =[]      
        passes_list[-1]['azimuth']   =[]
        passes_list[-1]['sublat']    =[]
        passes_list[-1]['sublong']   =[]
        passes_list[-1]['elevation'] =[]
        
        while tr < ts:
            obs.date = tr
            iss.compute(obs)
            
            passes_list[-1]['alt']      +=[math.degrees(iss.alt)] 
            passes_list[-1]['azimuth']  +=[math.degrees(iss.az)] 
            passes_list[-1]['sublat']   +=[math.degrees(iss.sublat)] 
            passes_list[-1]['sublong']  +=[math.degrees(iss.sublong)] 
            passes_list[-1]['elevation']+=[iss.elevation/1000.]
        
            tr = ephem.Date(tr + 20.0 * ephem.second)
        obs.date = tr + ephem.minute
    return passes_list

if __name__ == '__main__':
    next_passes(5, True)
        