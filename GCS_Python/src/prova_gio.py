def float_2_bytes():
    '''
    This function is used to convert float into bytes
    for using with command sending
    '''
    import random
    import struct

    floatlist = [0.5533]
    buf = struct.pack('%sf' % len(floatlist), *floatlist)
    print len(buf)
    for ch in buf:
        print (ord(ch))
    
float_2_bytes()