'''
Created on 10/mar/2014

@author: Lorenzo Feruglio, Paolo Muraro
'''
import random
import struct
from time import localtime, strftime, sleep
from pylab import * 
from dict_telemetry import create_telemetry_dict

debug = False

def convert(graphics, graph_dict=None, input_string=None):

    if type(graphics).__name__=='list':
        tmp=graphics
        graphics=tmp[0]
        graph_dict=tmp[1]
        input_string=tmp[2]
    if graph_dict==None:
        graph_dict, useless = create_telemetry_dict()
    file2=open('testHXD.txt','w')

    '''
    Convert the ASCII file in a hex file
    TODO: This initial part could be improved, by using the chr() method instead
    of writing to file...
    '''
    if input_string[:3] == 'all':
        path = input_string[3:]
        file1=open(path+'/packets.txt','rb')
    else:
        try:
            file1=open('packet.txt','rb')
        except:
            file1=open('packet.txt','w')
            file1.close()
    support = []
    
    if input_string!=None and len(input_string)==113:
        file1.close()
        file1=open('packet.txt','w')
        file1.write(input_string)
        file1.close()
        file1=open('packet.txt','rb')
        
    while 1:
        char = file1.read(1)
        if not char: break
        support += char
    for count in range(len(support)):
        file2.write(support[count].encode("hex"))
        if count != len(support)-1:
            file2.write(' ')
    file1.close()
    file2.close()
        
    '''
    Convert the hex file in the decimal file
    Packets are separated by spaces
    #TODO: could be improved'''

    file3=open('testHXD.txt', 'r')
    file4=open('risultati.txt', 'w')

    n=0
    loop=0
    
    for line in file3:
        for val in line.split(' '):
            try:
                dec = int(val,16)
                if n<113:
                    file4.write(str(dec)+' ')
                if n==113:
                    file4.write('\n')
                    file4.write(str(dec)+' ')
                    n=0
                    loop+=1
                n=n+1
            except:
                print('problem in val: ' + str(val) + ' pos '+ str(loop*113+n))
    file4.close()
    file3.close()
    
    '''
    The third part here converts the decimal values in the final values for the graphs
    #TODO: could be improved by using ord()
    '''
    file5=open('risultati.txt', 'rb')
    file6=open('risultati_convertiti.txt', 'w')

    
    if graphics==False:
        for line in file5:
            var_list = line.split(' ')
            for cat in graph_dict.keys():
                for subcat in graph_dict[cat].keys():
                    try:
                        if graph_dict[cat][subcat].has_key('conversion_dict'):
                            value_list = []
                            for count in range(len(graph_dict[cat][subcat]['conversion_dict']['cont'])):
                                idx = graph_dict[cat][subcat]['conversion_dict']['cont'][count]
                                if graph_dict[cat][subcat]['conversion_dict']['var_func'][count]!=None:
                                    value_list += [graph_dict[cat][subcat]['conversion_dict']['var_func'][count](var_list[idx])]
                                else:
                                    value_list += [int(var_list[idx])]
                            graph_dict[cat][subcat]['value'] = graph_dict[cat][subcat]['conversion_dict']['func'](value_list)
                        else:
                            if debug:
                                print cat, subcat, 'nothing to convert'
                    except:
                        print('To complete: ' + cat + ', ' + str(subcat))
                fp=open('../data/release_time.txt')
                lines=fp.readlines()
                date = lines[1]
                graph_dict['STATUS'][2]['value']=str(date)
    graph_var = []
    charge_map={'charge':1,
                'discharge':-1,
                'nothing!':0}
    if graphics==True:  
        if graph_dict.has_key('value'):
            #only a particular key has been passed
            for line in file5:
                var_list = line.split(' ')
                try:
                    if graph_dict.has_key('conversion_dict'):
                        value_list = []
                        for count in range(len(graph_dict['conversion_dict']['cont'])):
                            idx = graph_dict['conversion_dict']['cont'][count]
                            if graph_dict['conversion_dict']['var_func'][count]!=None:
                                value_list += [graph_dict['conversion_dict']['var_func'][count](var_list[idx])]
                            else:
                                value_list += [int(var_list[idx])]
                            #print value_list
                        tmp = [graph_dict['conversion_dict']['func'](value_list)]
                        if graph_dict['name'] in ['BAT 1-Status','BAT 2-Status']:
                            tmp=charge_map[graph_dict['conversion_dict']['func'](value_list)]
                            graph_var+=[tmp]
                        else:
                            graph_var+=tmp
                        #print graph_var
                    else:
                        print graph_dict['name'], 'nothing to convert'
                except:
                    print('Error in ' + graph_dict['name'])
            graph_dict['plot']=graph_var
        else:
            for cat in graph_dict.keys():
                    for subcat in graph_dict[cat].keys():
                        graph_dict[cat][subcat]['graph_list'] = []
            #we need to plot all the graphs
            for line in file5:
                var_list = line.split(' ')
                for cat in graph_dict.keys():
                    for subcat in graph_dict[cat].keys():
                        try:
                            if graph_dict[cat][subcat].has_key('conversion_dict'):
                                value_list = []
                                for count in range(len(graph_dict[cat][subcat]['conversion_dict']['cont'])):
                                    idx = graph_dict[cat][subcat]['conversion_dict']['cont'][count]
                                    if graph_dict[cat][subcat]['conversion_dict']['var_func'][count]!=None:
                                        value_list += [graph_dict[cat][subcat]['conversion_dict']['var_func'][count](var_list[idx])]
                                    else:
                                        value_list += [int(var_list[idx])]
                                    #print value_list
                                #print graph_dict[cat][subcat]['conversion_dict']['func'](value_list)
                                graph_dict[cat][subcat]['graph_list'] += [graph_dict[cat][subcat]['conversion_dict']['func'](value_list)]
                                #print cat, subcat, graph_dict[cat][subcat]['graph_list']
                            else:
                                print cat, subcat, 'nothing to convert'
                        except:
                            print('To complete: ' + cat + ', ' + str(subcat))
                            
       
    file6.close()
    file5.close()

    return graph_dict

def float_2_bytes(angle):
    '''
    This function is used to convert float into bytes
    for using with command sending
    '''
    floatlist=[angle]
    output=[]
    tmp_output = struct.pack('%sf' % len(floatlist), *floatlist)
    for ch in tmp_output:
        output+=[ch]
    return output


if __name__ == '__main__':
    print 'cannot use the file as main'
    #d , u= create_telemetry_dict()
    #convert(True, d['EPS'][11], 'all')
    #convert(True, None, 'all')
    #convert(False, None)