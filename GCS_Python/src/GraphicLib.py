'''
Created on 18/giu/2014

@author: Lorenzo Feruglio, Giovanna Rinaldi
'''
import wx
import platform
import threading
from convert import convert

class GraphicThread(threading.Thread):
    def __init__(self, graphics=True, key=None, input_string=None, telemetry_dict=None):
        """
        @param parent: The gui object that should recieve the value
        @param value: value to 'calculate' to
        """
        self.graphics=graphics
        self.key=key
        self.input_string=input_string
        self.telemetry_dict=telemetry_dict
        threading.Thread.__init__(self)
        

    def run(self):
        """Overrides Thread.run. Don't call this directly its called internally
        when you call Thread.start().
        TODO: we need to convert this sleep(1) in a -> serial read (block) and a queue.get (block)
        """
        convert(self.graphics, self.key, self.input_string, self.telemetry_dict)
     
def CallGraphicThread(arg1):
    if type(arg1).__name__=='instance':
        data=arg1.data
        graphics=data[0]
        key=data[1]
        input_string=data[2]
        telemetry_dict=data[3]
        t = GraphicThread(graphics, key, input_string, telemetry_dict)
        t.start() 

def WindowSizeAndPosition(Window_Name, b=None, h=None):
    '''
    '''
    if b==None and h==None:
        (b,h)=wx.GetDisplaySize()
    
    coeff = 1
    if platform.system() == 'Darwin':
        coeff = 0.9
    
    if Window_Name=='Telemetry':
        return {'size':(b*3/4,coeff*h*3/5),'pos':(0,VerticalShift())}
    elif Window_Name=='Command':
        return {'size':(b*3/4,coeff*h/3),'pos':(0,coeff*h*3/5+VerticalShift()+40)}
    elif Window_Name=='Map':
        return {'size':(b/3,coeff*h),'pos':(0,VerticalShift())}
    elif Window_Name=='Console':
        return {'size':(b*1/4,coeff*h/3+30),'pos':(b*3/4,coeff*h*3/5+VerticalShift()+40)}
    elif Window_Name=='Size':
        return {'size':(b,h),'pos':(0,VerticalShift())}
    elif Window_Name=='Clock':
        return {'size':(b*1/4,h/5),'pos':(b*3/4,0)}
    elif Window_Name=='Splash':
        return {'size':(310,310),'pos':((b-310)/2,(h-310)/2)}
    
def VerticalShift():
    '''
    '''
    if platform.system() == 'Darwin':
        return 20
    else:
        return -10