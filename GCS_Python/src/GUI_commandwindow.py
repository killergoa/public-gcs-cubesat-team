'''
Created on 18/giu/2014

@author: Lorenzo Feruglio, Giovanna Rinaldi
'''
import wx
import platform
import math
from wx.lib.pubsub import setupkwargs             # only in app's startup module
from wx.lib.pubsub import pub as Publisher      # in all modules that use pubsub
#import wx.lib.inspection
 
from dict_command_GUI_prop import create_command_dict, create_window_items_dict
from dict_command_strings import * 
from dict_conversions import opmode_map
from GraphicLib import WindowSizeAndPosition
from GUI_telemetry import *
from convert import float_2_bytes
from time import localtime, strftime, sleep
from resources import resource_path
len_command = 43

command_dict, command_list,tris_button_dict = create_command_dict()
command_dict_function = create_command_dict_function()


MAX_COL_DIM=5

class CommandTimeBox(wx.Panel): 
    
    ''' This class has been created to reproduce the TextCtrl+Label in C''' 
    # this class is a container that defines label, textcontrol and that allows to create
    # multiple telemtrybox objects
    def __init__(self, parent, label, value):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY) 
        gsiz2 = wx.BoxSizer(wx.VERTICAL)
        self.label_text = wx.Button(self, id=-1, pos=(10,0), label=label)
        gsiz2.Add(self.label_text, 2, wx.EXPAND)
        
        self.text_control = wx.TextCtrl(self, id=-1, value=value)
        gsiz2.Add(self.text_control, 2, wx.EXPAND)
        self.SetSizer(gsiz2)

class CommandBox(wx.Panel): 
    
    '''
    This class has been created to have a repeatable object composed by a TextControl
    and by a button.
    ''' 
    def __init__(self, parent, button_name, description):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY) 
        hsiz = wx.BoxSizer(wx.HORIZONTAL)
        gsiz2 = wx.BoxSizer(wx.VERTICAL)
        self.label_text = wx.StaticText(self, id=-1, pos=(10,0), label=description)
        gsiz2.Add(self.label_text,1, wx.EXPAND)
        self.text_control = wx.TextCtrl(self,id=-1, value='waiting')
        gsiz2.Add(self.text_control,2, wx.EXPAND)
        
        self.button = wx.Button(parent=self, label=button_name, id=-1)
        hsiz.Add(gsiz2, 2, wx.EXPAND)
        hsiz.Add(self.button, 1, wx.EXPAND)
        self.SetSizer(hsiz)


class CommandWindow(wx.Frame):
    '''
    This class defines the construction and behaviour of the command window
    '''
    def __init__(self,parent,id,title, queue):
        wx.Frame.__init__(self,parent,id,title, size = WindowSizeAndPosition('Command')['size'])
        '''DIMENSIONI IN ROSSO DELLA FINESTRA DA CREARE'''
        favicon = wx.Icon(resource_path('cube.ico','img'),
                           wx.BITMAP_TYPE_ICO)
        self.SetIcon(favicon)
        self.command_name = None
        self.last_command = None
        self.transponder_update = None
        self.command_queue = queue
        self.previous_mode = None
        #command chain variables
        self.user_input=None
        self.op_mode = 'Basic mission'
        self.command_list= []
        #we set the MyGUI frame centered on screen
        self.SetPosition(WindowSizeAndPosition('Command')['pos'])
        #create the dictionary of all the window items
        self.window_item_dict = create_window_items_dict(self)
        #create the sizer outside the cycles
        hboxsizer = wx.BoxSizer(wx.HORIZONTAL)
        #we create a k value to store the IDs of the buttons... could be deprecated      
        for category in command_list:
            #vertical sizers for each column of the window
            vboxsizer=wx.BoxSizer(wx.VERTICAL)
            for box_n in range(1, len(command_dict[category].keys())+1):
                if type(command_dict[category][box_n]).__name__ == 'str':
                    name = command_dict[category][box_n]
                    if self.window_item_dict[name]['type']=='command_box':
                        self.window_item_dict[name]['object'] = CommandBox(parent=self,
                                                                           button_name=self.window_item_dict[name]['button_name'],
                                                                           description=self.window_item_dict[name]['description']) 
                        vboxsizer.Add(self.window_item_dict[name]['object'], 1, wx.EXPAND)
                    elif self.window_item_dict[name]['type']=='button':
                        self.window_item_dict[name]['object'] = wx.Button(parent=self,
                                                                          label=self.window_item_dict[name]['button_name'],
                                                                          id=-1)
                        vboxsizer.Add(self.window_item_dict[name]['object'],1,wx.EXPAND)
                    elif self.window_item_dict[name]['type']=='infobox':
                        self.window_item_dict[name]['object'] = CommandTimeBox(parent=self,
                                                                          label=self.window_item_dict[name]['button_name'],
                                                                          value='no command has been sent yet')
                        vboxsizer.Add(self.window_item_dict[name]['object'],1,wx.EXPAND)
                elif type(command_dict[category][box_n]).__name__ == 'dict':
                    hbox = wx.BoxSizer(wx.HORIZONTAL)
                    row_dict = command_dict[category][box_n]
                    for count in range(1,len(row_dict.keys())+1):
                        name = command_dict[category][box_n][count]
                        self.window_item_dict[name]['object'] = wx.Button(parent=self,
                                                                          label=row_dict[count],
                                                                          id=-1)
                        hbox.Add(self.window_item_dict[name]['object'], 1, wx.EXPAND)
                    vboxsizer.Add(hbox,1,wx.EXPAND)
            if category != 'colonna3':
                hboxsizer.Add(vboxsizer,2.5,wx.EXPAND)
            else:
                hboxsizer.Add(vboxsizer,1,wx.EXPAND)
        self.SetSizer(hboxsizer)
        
        #disable the buttons
        for btn in self.window_item_dict.keys():
            if self.window_item_dict[btn].has_key('disable') and self.window_item_dict[btn]['disable']==True:
                self.window_item_dict[btn]['object'].button.Disable()
        
        #we bind events to the buttons
        self.Bind_Events()
        #subscribe to the transponder channel
        Publisher.subscribe(self.PopTransponder, 'trasponder')
        Publisher.subscribe(self.UpdateOpMode, 'Operative Mode Update')
        self.Show(True)
    
    def GetUserInput(self, evt):
        #we get the name of the button that was pressed (and generated the event)
        self.command_name = evt.GetEventObject().GetLabel()
        #updating the transponder_update attribute, for later use when we will
        #receive the transponder
        self.transponder_update = self.command_name
        if self.window_item_dict[self.command_name]['type'] == 'command_box':
            self.user_input = self.window_item_dict[self.command_name]['object'].text_control.GetValue() 
    
    def Check_Commands(self):
        '''
        This function is a wrapper to build the correct string to feed  to the
        TNC,  through a serial port write.
        OUTPUT: string (the command itself)
        '''
        if self.window_item_dict[self.command_name]['type'] == 'command_box':
            if not self.CheckUserInput():
                self.Dialog('Wrong command format: check input.','Error','ok') 
                return None
        if not self.CheckMode():
            'Cannot send the command!'
            return None
        #making a custom message appear, if present
        self.CustomMessage()
        #we check if the command  selected needs a confirmation before being sent
        if not self.AskConfirmation():
            #Operator has clicked CANCEL
            print 'Command not sent!'
            return False
        else:
            return True
    
    def Build_Commands(self, evt):
        '''
        Generic function to build the command string that will be sent to the TNC
        '''
        self.GetUserInput(evt)
        if self.Check_Commands():
            #at this point self.user_input contains the string written by the user
            self.window_item_dict[self.command_name]['function'](evt)
            command_list = self.ReturnCommandList()
            #here we chechk that the user input correctly matches the command list:
            #if there are two commands in the list, we expect to have a list of 
            #two elements in the user input.
            if self.CheckConsistencyUserInput(command_list):
                count=0
                for cmd in command_list:
                    tmp_user_input=self.ExtractPartOfUserInput(count)
                    self.MountCommandString(cmd,tmp_user_input)
                    self.Send_Commands()
                    self.SetCommandTime()
                    self.CheckManualOpMode()
                    count+=1
                self.UpdateLogger()
            else:
                print ('Error in consistency')
        self.last_command = self.command_name
        self.VariableCleanUp()
    
    def SetCommandTime(self):
        date = strftime("%a, %d %b %Y %H:%M:%S", localtime()).split(' ')
        newdate = date[4]
        self.window_item_dict['CommandTime']['object'].text_control.SetValue(newdate)
    
    def UpdateOpMode(self,mode):
        if type(mode[0]).__name__=='int':
            self.op_mode=opmode_map[mode[0]]
        else:
            self.op_mode = mode[0]
        print ('GUICMD: Setting opmode: ' + self.op_mode)
            
    def UpdateLogger(self):
        wx.CallAfter(Publisher.sendMessage, 'commands.txt', msg=[self.command_name])
            
    def ExtractPartOfUserInput(self, count):
        if self.window_item_dict[self.command_name].has_key('commands needed'):
            return self.user_input[count]
        else:
            return None
            
    def CheckConsistencyUserInput(self, command_list):
        if type(self.user_input).__name__ not in ['list','NoneType']:
            print('Error: self.user_input not formatted correctly in ' + self.command_name)
            return False
        else:
            if self.window_item_dict[self.command_name].has_key('commands needed'):
                if self.window_item_dict[self.command_name]['commands needed']!=len(self.user_input):
                    print('Error: command list not matching self.user_input')
                    return False
        return True
    
    def CheckManualOpMode(self):
        '''
        this function is used to manually set the OpMode of the GCS
        in case the transponder isn't received.. for example with save energy
        '''
        if self.window_item_dict[self.command_name].has_key('store opmode'):
            self.previous_mode = self.op_mode
        if self.window_item_dict[self.command_name].has_key('manual opmode'):
            if self.Dialog('Has the satellite entered '+self.command_name+' Mode?', 'Manually Set OpMode', 'yes_no'):
                if self.window_item_dict[self.command_name]['sets mode'] == 'previous mode':
                    wx.CallAfter(Publisher.sendMessage, 'Operative Mode Update', mode=[self.previous_mode])
                else:
                    mode = opmode_map[self.window_item_dict[self.command_name]['sets mode']]
                    wx.CallAfter(Publisher.sendMessage, 'Operative Mode Update', mode=[mode])
            else:
                self.Dialog('Please consider sending '+self.command_name+
                            ' Command again.\nIt looks as it has not been received.\n',
                            'Warning','ok')
                    
    def PopTransponder(self, msg):
        '''
        This function creates a popup when the tranponder is received.
        '''
        date = strftime("%a, %d %b %Y %H:%M:%S", localtime()).split(' ')
        newdate = date[4]
        if self.last_command!= 'SMS':
            self.Dialog('Trasponder ricevuto alle '+newdate,'Info','ok')
        else:
            self.Dialog('Trasponder ricevuto alle '+newdate+':\n\t\t'+msg[0], 'Info', 'ok')
        if self.window_item_dict[self.transponder_update].has_key('sets mode'):
            try:
                self.op_mode = opmode_map[self.window_item_dict[self.transponder_update]['sets mode']]
                wx.CallAfter(Publisher.sendMessage, 'Operative Mode Update', mode=[self.op_mode])
            except:
                print('Error, tried to update operative mode for a command that should not do it\n')
        self.transponder_update = None
        
    def MountCommandString(self, cmd, tmp_user_input=None):
        if tmp_user_input==None:
            if self.user_input!=None:
                #self.user_input deve essere una lista di caratteri 
                cmd['value'] = self.user_input
            else:
                cmd['value'] = []
        else:
            cmd['value']=tmp_user_input
        chars_left = len_command-len(cmd['header'])-1-len(cmd['value'])
        tmp_str = cmd['value']+['\x00']*chars_left
        self.command_string=cmd['header']+''.join(tmp_str)+cmd['closer']
    
    def Send_Commands(self):
        '''
        '''
        print ('Sending --> ' + self.command_name + ' <-- command')
        self.command_queue.put(self.command_string)
    
    def VariableCleanUp(self):
        self.user_input = None
        self.command_name = None
        self.command_list = []
            
    def CheckUserInput(self):
        try:
            if self.window_item_dict[self.command_name].has_key('expected_input'):
                if (self.window_item_dict[self.command_name]['expected_input']['type'] != 'str' and
                    (len(self.user_input.split(' ')) != self.window_item_dict[self.command_name]['expected_input']['len'])):
                    return False
                elif (self.window_item_dict[self.command_name]['expected_input']['type'] == 'int' and
                     type(eval(self.user_input)).__name__ != 'int'):
                    return False
            return True
        except:
            return False        
            
    def CustomMessage(self):
        if self.window_item_dict[self.command_name].has_key('custom message'):
            dlg=wx.MessageDialog(self, self.window_item_dict[self.command_name]['custom message'],
                                 'Info', wx.OK|wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
        return True

    def Dialog(self, label, text, options):
        if options == 'ok_cancel':
            opt = wx.OK|wx.CANCEL|wx.ICON_QUESTION
        elif options == 'ok':
            opt = wx.OK|wx.ICON_INFORMATION
        elif options == 'yes_no':
            opt = wx.YES_NO|wx.ICON_QUESTION
        dlg=wx.MessageDialog(self, label,
                         text, opt) 
        result=dlg.ShowModal()
        dlg.Destroy()
        if result == wx.ID_OK or result == wx.ID_YES:
            return True
        else:
            return False
        
    def CheckMode(self):
        if self.window_item_dict[self.command_name].has_key('required mode'):
            if self.op_mode in self.window_item_dict[self.command_name]['required mode']:
                print 'Operative mode allows for this command'
                return True
            else:
                self.Dialog('You are in the following mode: ' + self.op_mode + '.\n'+
                            'You are not in the required operative mode for this command.\n'+
                            'Please return in one of these modes:\n' +
                            str(self.window_item_dict[self.command_name]['required mode']),
                            'Error', 'ok')
                return False
        else:
            #the command can be sent from any operative mode
            return True
            
    def AskConfirmation(self):
        if (self.window_item_dict[self.command_name].has_key('needs confirm') and 
            self.window_item_dict[self.command_name]['needs confirm'] == True):
            if self.Dialog('Are you sure?', 'Confirm Command', 'ok_cancel'):
                return True
            else:
                return False
        else:
            #we return True if a confirmation wasn't needed
            return True
        
    def ReturnCommandList(self):
        '''
        This function is used to check if it's needed to send two commands or one
        '''
        if len(command_dict_function[self.command_name].keys()) == 2:
            command_list=[]
            for key in command_dict_function[self.command_name].keys():
                command_list += [command_dict_function[self.command_name][key]]
            return command_list
        elif len(command_dict_function[self.command_name].keys()) == 3:
            if self.command_name == 'ADCS mode':
                if ord(self.user_input[0]) == 0:
                    return [command_dict_function[self.command_name]['A']]
                elif ord(self.user_input[0]) == 1:
                    return [command_dict_function[self.command_name]['B']]
                elif ord(self.user_input[0]) == 2:
                    return [command_dict_function[self.command_name]['C']]
            else:
                return [command_dict_function[self.command_name]]
        else:
            return [command_dict_function[self.command_name]]
  
    def Update_MissionTime(self, evt):
        missiontime = []
        for item in str(self.user_input).split(' '):
            missiontime += [int(item)]
        centiseconds = missiontime[0] * 3600 * 24 * 100 
        centiseconds += missiontime[1] * 3600 * 100
        centiseconds += missiontime[2] * 60 * 100
        centiseconds += missiontime[3] * 100
        
        interi_cents = []
        interi_cents += [int((centiseconds
                / (256 * 256 * 256)))]
        
        interi_cents += [int(((centiseconds
                - interi_cents[0] * (256 * 256 * 256))
                / (256 * 256)))]
        
        interi_cents += [int(((centiseconds
                - (interi_cents[0] * (256 * 256 * 256)
                        + interi_cents[1] * (256 * 256)))
                / (256)))]
        
        interi_cents += [((centiseconds
                - (interi_cents[0] * (256 * 256 * 256)
                        + interi_cents[1] * (256 * 256)
                        + interi_cents[2] * 256)))]
        
        tmp=[]
        for item in interi_cents:
            tmp += [chr(item)]
        self.user_input=tmp
#         com1a[20] = (char) interi_cents[0];
#         com1a[21] = (char) interi_cents[1];
#         com1a[22] = (char) interi_cents[2];
#         com1a[23] = (char) interi_cents[3];
        
    def Update_UTC(self, evt):
        '''
        '''    
        utc=[]
        utc_bytes=[[],[]]
        for item in str(self.user_input).split(' '):
            utc += [float(item)]
        count=0
        i=0
        for item in utc:
            count+=1
            utc_bytes[i]+=float_2_bytes(float(item))
            if count==3:
                i+=1
        self.user_input=utc_bytes
    
    def Update_orbit_parameters(self, evt):
        '''
        '''
        orbital_bytes = [[],[]]
        count=0
        i=0
        for item in str(self.user_input).split(' '):
            count+=1
            orbital_bytes[i]+=float_2_bytes(float(item))
            if count==4:
                i+=1
        self.user_input=orbital_bytes
        
    def Update_torquers_polarity(self, evt):
        '''
        '''
        polarity_map = {'+':0,
                        '-':1}
        tmp_command = self.user_input.split(' ')
        command_val = 4*polarity_map[tmp_command[0]]+\
                      2*polarity_map[tmp_command[1]]+\
                        polarity_map[tmp_command[2]]
        self.user_input = [chr(command_val)]
        print ('COMANDO ' + str(command_val))
        
    def Update_Gain(self, evt):
        '''
        '''
        gain_bytes=[[],[],[]]
        tmp_str = str(self.user_input).split(' ')
        tmp_in = []
        for item in tmp_str:
            tmp_in+=[int(item)]
        for count in range(len(tmp_in)):
            tmp=int(tmp_in[count]/(256*256))
            tmp_gain=[tmp]
            tmp=int((tmp_in[count]-tmp_gain[0]*256*256)/256)
            tmp_gain+=[tmp]
            tmp=tmp_in[count]-tmp_gain[0]*256*256-tmp_gain[1]*256
            tmp_gain+=[tmp]
            gain_bytes[count] += tmp_gain
        tmp=[]
        for l in gain_bytes:
            for item in l:
                tmp += [chr(item)]
        self.user_input=tmp
        
    def SMS(self, evt):
        tmp = []
        for ch in self.user_input:
            tmp += chr(ord(ch))
        self.user_input=tmp
        
    def Save_Energy(self, evt):
        pass
        
    def Live_again(self, evt):
        pass
  
    def Stop_TX(self, evt):
        pass
        
    def Download_telemetry(self, evt):
        '''
        '''
        splitted_input = self.user_input.split(' ')
        length = int(splitted_input[0])
        interval = int(splitted_input[1])
        tmp = []
        div = length / 100;
        resto = length % 100;
        tmp += [chr(div+48)]
        div = resto / 10;
        tmp += [chr(div+48)]
        resto = resto % 10;
        tmp += [chr(resto+48)]
        div = interval / 10;
        tmp += [chr(div+48)]
        resto = interval % 10;
        tmp += [chr(resto+48)]
        
        self.user_input =tmp
        
    def Set_preamble(self, evt):
        '''
        It's an integer between 1 and 255, converted into a char
        '''
        tmp=int(self.user_input)
        self.user_input = [chr(tmp)]      
        
    def Set_transmission_time(self, evt):
        '''
        It's an integer between 1 and 255, converted into a char
        '''
        tmp=int(self.user_input)
        self.user_input = [chr(tmp)]
  
    def Manage_ADCS(self, evt):
        tmp=int(self.user_input[0])
        self.user_input = [chr(tmp)]
        
    def Resume_TX(self, evt):
        pass
        
    def Reboot(self, evt):
        pass
        
    def Stop_reburn(self, evt):
        pass
  
    def Stop_CW(self, evt):
        pass
             
    def Bind_Events(self):    
        #bind events to buttons (the id matters)
        '''close window event'''
        self.Bind(wx.EVT_CLOSE,self.OnClose)
        '''creo gli eventi per ogni bottone'''
        for name in self.window_item_dict.keys():
            if self.window_item_dict[name]['type']=='command_box':
                self.Bind(wx.EVT_BUTTON,self.Build_Commands,
                          self.window_item_dict[name]['object'].button)
            if self.window_item_dict[name]['type']=='button':
                self.Bind(wx.EVT_BUTTON,self.Build_Commands,
                          self.window_item_dict[name]['object'])
                     
    def OnClose(self,evt=None):
        '''
        Terminate the app once the close button is pressed.
        '''
        print 'Stopping Finestra Comandi Thread...'
        self.Destroy()
        
#===============================================================================================
    
class MyOutputWindow(wx.PyOnDemandOutputWindow):
    def __init__(self) :
        wx.PyOnDemandOutputWindow.__init__( self )
        b = 1376
        h = 768
        
        self.pos =WindowSizeAndPosition('Console', b,h)['pos']
        self.size =WindowSizeAndPosition('Console', b,h)['size']
        self.title='Console'

                       
def start(communication_queue=None):
    '''
    This external function (not a method of any class) has been created in order to allow
    execution of this file as a standalone app (with the if check on name) or to call it from
    GUI program, on button press
    '''
    if communication_queue==None:
        print('expect wrong events, queue is none')
    CommandWindow(None, -1, 'Command Window interface Hackaton CubeSat Team', communication_queue)
    app.MainLoop()
    #wx.lib.inspection.InspectionTool().Show()

if __name__ == '__main__':
    
    wx.App.outputWindowClass = MyOutputWindow
    app = wx.App(redirect=False)
    start()  
    
