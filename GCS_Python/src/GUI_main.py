'''
Created on 06/nov/2013

@author: Lorenzo Feruglio
'''
import os, pickletools, subprocess, itertools, operator,\
       datetime, strop, math, FileDialog
import wx
import GUI_menu
import Queue
from GraphicLib import WindowSizeAndPosition
from error_messages import *
import Tkinter
from resources import resource_path

class MyApp(wx.App):
    def OnInit(self):
        MySplash = SplashScreen()
        MySplash.Show()
        return True
    
class MyOutputWindow(wx.PyOnDemandOutputWindow):
    def __init__(self) :
        wx.PyOnDemandOutputWindow.__init__(self)
        b = 1376
        h = 768
        self.pos = WindowSizeAndPosition('Console', b, h)['pos']
        self.size = WindowSizeAndPosition('Console', b, h)['size']
        self.title = 'Console'

class SplashScreen(wx.SplashScreen):
    '''
    This class shows an initial splashscreen for the GUI initialization.
    The Polito CubeSat Team logo is shown.
    '''

    def __init__(self, parent = None):
        '''
        Constructor for the splashscreen
        '''
        try:
            name = resource_path('cube.png','img')
            aBitmap = wx.Image(name).ConvertToBitmap()
            wx.GetDisplaySize()
            splashStyle = wx.SPLASH_CENTER_ON_PARENT | wx.SPLASH_TIMEOUT
            splashDuration = 500 #milliseconds
            # Call the constructor with the above arguments in exactly the lowwing order
            wx.SplashScreen.__init__(self,aBitmap, splashStyle, splashDuration, parent)
            wx.Yield()
        except:
            print('Could not load splashscreen')
            self.Bind(wx.EVT_CLOSE, self.OnExit)
        self.Bind(wx.EVT_CLOSE, self.OnExit)
            
    def OnExit(self,evt):
        self.Hide()
        MainFrame = GUI_menu.start()
        self.Destroy()
        
        

if __name__ == '__main__':
    wx.App.outputWindowClass = MyOutputWindow
    app = MyApp()
    app.MainLoop()