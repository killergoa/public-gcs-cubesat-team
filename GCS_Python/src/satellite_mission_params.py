opmode_map = { 0 :'Basic mission',
               1 :'Detumbling',
               2 :'Determination',
               3 :'Save energy',
               4 :'Silent'}

inverted_opmode_map = { 'Basic mission'         :0,
                        'Detumbling'            :1,
                        'Determination'         :2,
                        'Save energy'           :3,
                        'Silent'                :4}

correct_sig = ['C','U','B','E','S','A','T','T','E','A','M']
packet_header = ['\xC0','\x00','\x82','\x98','\x98','\x82','\x98','\x98','\xE0',
                 '\x8A','\xA6','\xA8','\x82','\xA4','\x40','\x62','\x03','\xF0']
