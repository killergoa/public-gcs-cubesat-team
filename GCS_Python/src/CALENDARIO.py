'''
Created on 19/giu/2014

@author: Giovanna Rinaldi
'''
import wx
import wx.calendar as cal

class Calendar(wx.Dialog):
    def __init__(self, parent, id, title):
        wx.Dialog.__init__(self, parent, id, title)

        vbox = wx.BoxSizer(wx.VERTICAL)

        calend = cal.CalendarCtrl(self, -1, wx.DateTime_Now(),
            style = cal.CAL_SHOW_HOLIDAYS|cal.CAL_SEQUENTIAL_MONTH_SELECTION)
        vbox.Add(calend, 0, wx.EXPAND | wx.ALL, 20)
        self.Bind(cal.EVT_CALENDAR, self.OnCalSelected, id=calend.GetId())

        vbox.Add((-1, 20))

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.text = wx.StaticText(self, -1, 'Date')
        hbox.Add(self.text)
        vbox.Add(hbox, 0,  wx.LEFT, 8)

        self.Bind(wx.EVT_CLOSE, self.OnQuit)

        self.SetSizerAndFit(vbox)

        self.Show(True)
        self.Centre()


    def OnCalSelected(self, event):
        date = event.GetDate()
        dt = str(date).split(' ')
        s = ' '.join(str(s) for s in dt)
        self.text.SetLabel(s)

    def OnQuit(self, event):
        self.Destroy()

def start():
    app = wx.App()
    Calendar(None, -1, 'Calendar')
    app.MainLoop()
    
if __name__ == '__main__':
    start()
    