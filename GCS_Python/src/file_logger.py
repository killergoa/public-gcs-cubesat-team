'''
Created on 01/set/2014

@author: Lorenzo Feruglio
'''
import threading
import time
from time import localtime, strftime
from wx.lib.pubsub import setuparg1             # only in app's startup module
from wx.lib.pubsub import pub as Publisher      # in all modules that use pubsub
from prettytable import PrettyTable

DEBUG = False

class LoggerClass(threading.Thread):
    '''
    This class is the logger class, which user a publisher/subscribe architecture
    to provide all logging information
    '''
    
    def __init__(self, path, option):
        '''
        Init method
        '''
        self.path=path
        #resetting the files if the option is 'write'
        if option=='w':
            print 'Resetting log files\n'
            files=[]
            files+=[open(self.path+'/commands.txt',option)]
            files+=[open(self.path+'/packets.txt',option)]
            files+=[open(self.path+'/trasponders.txt',option)]
            files+=[open(self.path+'/activity.txt',option)]
            files+=[open(self.path+'/conversions.txt', option)]
            
            for f in files:
                f.close()
        #always append, file are already cleared if that was needed
        self.option = 'a'
        
        Publisher.subscribe(self.LogCommands, 'commands.txt')
        Publisher.subscribe(self.LogPackets, 'packets.txt')
        Publisher.subscribe(self.LogTrasponders, 'trasponders.txt')
        Publisher.subscribe(self.LogConversions, 'conversions.txt')
        
        self.Close_Signal=False
        threading.Thread.__init__(self)
        
    def run(self):
        '''
        Overrides Thread.run. Don't call this directly its called internally
        when you call Thread.start().
        TODO: we need to convert this sleep(1) in a -> serial read (block) and a queue.get (block)
        '''
        while not self.Close_Signal:
            if DEBUG:
                print 'Logger thread alive'
            time.sleep(1)
        print 'Stopping Logger Thread...'
        return None
    
    def signal_stop(self):
        self.Close_Signal=True
        Publisher.unsubAll()
        
    def LogConversions(self, msg):
        '''
        Saves in a file all the informations about the converted packet
        '''
        try:
            self.conversions=open(self.path+'/conversions.txt',self.option)
            d = msg[0]
            self.conversions.write('#'*33+'\n\tPACKET NUMBER '+
                                   str(d['STATUS'][1]['value'])+'\n'+'#'*33+'\n')
            for cat in d.keys():
                t = PrettyTable([cat, 'value'])
                i = 0
            
                for key, val in d[cat].items():
                    t.add_row([d[cat][key]['name'], str(d[cat][key]['value'])])
                i+=1;
                self.conversions.write(t.get_string())
                self.conversions.write('\n')
            self.conversions.close()
        except:
            print ('Error in logging conversions!')

            
                    
           
    def LogCommands(self,msg):
        self.commands=open(self.path+'/commands.txt',self.option)
        time_str=strftime("%Y-%m-%d %H:%M:%S", localtime())
        output=time_str+ ' -> Command Sent ' + str(msg[0])+ '\n'
        self.commands.write(output)
        self.commands.close()
        self.LogActivity('command',msg[0])
    
    def LogPackets(self,msg):
        self.packets=open(self.path+'/packets.txt',self.option)
        output=msg[0]
        self.packets.write(output)
        self.packets.close()
        self.packets=open('logpackets.txt',self.option)
        output=msg[0]
        self.packets.write(output)
        self.packets.close()
        self.packets=open('packet.txt','w')
        output=msg[0]
        self.packets.write(output)
        self.packets.close()
        self.LogActivity('packet',msg[0])
        
    def LogTrasponders(self,msg):
        self.trasponders=open(self.path+'/trasponders.txt',self.option)
        output=msg[0]
        self.trasponders.write(output)
        self.trasponders.close()
        self.LogActivity('trasponder',None)
        
    def LogActivity(self,data_type, msg):
        self.activity=open(self.path+'/activity.txt',self.option)
        time_str=strftime("%Y-%m-%d %H:%M:%S", localtime())
        if data_type=='command':
            output=time_str+ ' -> Command Sent ' + str(msg)+ '\n'
        elif data_type=='packet':
            output=time_str+ ' -> Packet Received ' + str(msg) + '\n'
        elif data_type=='trasponder':
            output=time_str+ ' -> Transponder received\n'
        self.activity.write(output)
        self.activity.close()

    