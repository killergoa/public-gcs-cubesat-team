'''
Created on 09/giu/2014

@author: Lorenzo Feruglio
'''

#all the functions here shall return two strings, one for the command name,
#one with all the command data

def Reverse_Update_MissionTime(obj, data):
    interi_cents=[]
    for ch in data:
        interi_cents+=[ord(ch)]
    centiseconds=0
    for count in range(len(interi_cents)):
        centiseconds+=interi_cents[count]*(256**count)
    missiontime=[]
    missiontime+=[int(centiseconds/(3600*24*100))]
    centiseconds=centiseconds-missiontime[0]*3600*24*100
    missiontime+=[int(centiseconds/(3600*100))]
    centiseconds=centiseconds-missiontime[1]*3600*100
    missiontime+=[int(centiseconds/(60*100))]
    centiseconds=centiseconds-missiontime[2]*60*100
    missiontime+=[int(centiseconds/(100))]
    output=str(missiontime[0])+\
           ' '+str(missiontime[1])+\
           ' '+str(missiontime[2])+\
           ' '+str(missiontime[3])
    print output
    return output
        

def Reverse_Update_UTC(obj, data):
    pass

def Reverse_Update_orbit_parameters(obj, data):
    pass
def Reverse_Manoeuver(obj, data):
    pass
def Reverse_SMS(obj, data):
    pass
def Reverse_Save_Energy(obj, data):
    pass
def Reverse_Live_again(obj, data):
    pass
def Reverse_Stop_TX(obj, data):
    pass
def Reverse_Download_telemetry(obj, data):
    pass
def Reverse_Set_preamble(obj, data):
    pass
def Reverse_Set_transmission_time(obj, data):
    pass
def Reverse_Manage_ADCS(obj, data):
    pass
def Reverse_Resume_TX(obj, data):
    pass
def Reverse_Reboot(obj, data):
    pass

def Reverse_Stop_reburn(obj, data):
    pass

def Reverse_Stop_CW(obj, data):
    pass
