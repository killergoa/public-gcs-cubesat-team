'''
Created on 24/maggio/2014

@author: Lorenzo Feruglio
'''
import wx
import wx.lib.inspection
import GUI_commandwindow
from digital_clock import LED_clock
from dict_telemetry import create_telemetry_dict
from dict_command_GUI_prop import create_command_dict
from dict_limits import create_limit_dict
from dict_command_strings import * 
from GraphicLib import *
import sys
# from convert import convert
import digital_clock
import CALENDARIO 
import tetris
from thread_serial import SerialReadingThread
import Queue
import os
from time import localtime, strftime, sleep
from graphics import graphs
from file_logger import LoggerClass 
from prettytable import PrettyTable
import GUI_passes
from resources import resource_path

from satellite_mission_params import opmode_map, inverted_opmode_map

from wx.lib.pubsub import setupkwargs  # only in app's startup module
from wx.lib.pubsub import pub as Publisher  # in all modules that use pubsub
 
# import thread_serial 
# import Queue #importa la coda da python

class TelemetryBox(wx.Panel): 
    
    ''' This class has been created to reproduce the TextCtrl+Label in C''' 
    # this class is a container that defines label, textcontrol and that allows to create
    # multiple telemtrybox objects
    def __init__(self, parent, obj_dict, path):
        self.path = path
        self.obj_dict = obj_dict
        wx.Panel.__init__(self, parent, id=wx.ID_ANY) 
        gsiz2 = wx.BoxSizer(wx.VERTICAL)
        self.button = wx.Button(parent=self, label=obj_dict['name'], id=-1)
        gsiz2.Add(self.button, 2, wx.EXPAND)
        
        self.text_control = wx.TextCtrl(self, id=-1, value='waiting')
        self.status_panel = wx.Panel(self, wx.ID_ANY, style=wx.BORDER_STATIC)
        gsiz2.Add(self.text_control, 2, wx.EXPAND)
        gsiz2.Add(self.status_panel, 1, wx.EXPAND)
        self.SetSizer(gsiz2)
        self.text_button = obj_dict['name']
        self.button.Bind(wx.EVT_BUTTON, self.gse)

    def gse(self, evt):
        wx.CallAfter(Publisher.sendMessage, 'graphdemo', msg=[True, self.obj_dict, 'all'+self.path])
                             

MAX_ROW_DIM = 10
MAX_COL_DIM = 5

class MyGUI(wx.Frame):
    def __init__(self, parent, id, title, path, option, mode, initial_window_obj):
        wx.Frame.__init__(self, parent, id, title, size=WindowSizeAndPosition('Telemetry')['size'])
        '''DIMENSIONI IN ROSSO DELLA FINESTRA DA CREARE'''
        favicon = wx.Icon(resource_path('cube.ico','img'),
                           wx.BITMAP_TYPE_ICO)
        self.SetIcon(favicon)
        self.NA_flag = False
        self.path = path
        self.initial_window_obj = initial_window_obj
        # queue initialization
        self.command_queue = Queue.Queue()
        self.telemetry_queue = Queue.Queue()
        self.GUI_commandwindow = None
        self.telemetry_dict, self.telemetry_list = create_telemetry_dict()
        self.command_dict, self.command_list, self.tris_button = create_command_dict()
        #self.limit_dict = create_limit_dict() 
        self.opmode_window = None
        self.op_mode = 'Basic mission'
        # starting the threads for serial communication

        # Binding for the update message by the publisher.
        # This library is used to handle the GUI updating without
        # encountering memory problems that happen when another thread
        # tries to update the GUI
        Publisher.subscribe(self.UpdateRoutine, 'update')
        Publisher.subscribe(self.UpdateNA, 'update_NA')
        Publisher.subscribe(graphs, 'graphdemo')
        Publisher.subscribe(self.UpdateOpMode, 'Operative Mode Update')
        self.serial_thread = SerialReadingThread(self, mode)
        self.serial_thread.start()
        self.graphsThread = None
        sizer_map = dict() 
        for category in self.telemetry_list:
            sizer_map[category] = {'siz':wx.GridSizer(self.Return_Dim(category, 'telemetry'), MAX_ROW_DIM, 0, 0),
                                    'prop':self.Return_Dim(category, 'telemetry')}
            
        vboxsizer = wx.BoxSizer(wx.VERTICAL)  
               
        self.qtool = wx.ToolBar(self)
        
        self.qtool.AddSimpleTool(99, wx.Image(resource_path('CUBESAT.png','img'), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        if mode != 'Peer':
            self.qtool.AddSimpleTool(98, wx.Image(resource_path('comando2.png','img'), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.qtool.AddSimpleTool(97, wx.Image(resource_path('CLOCK.png','img'), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.qtool.AddSimpleTool(96, wx.Image(resource_path('CALENDARIO.png','img'), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.qtool.AddSimpleTool(94, wx.Image(resource_path('LOG.png','img'), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.qtool.AddSimpleTool(93, wx.Image(resource_path('opmode.png','img'), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        self.qtool.AddSimpleTool(92, wx.Image(resource_path('passes.png','img'), wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        #self.qtool.AddSimpleTool(92, wx.Image('../img/emergencymode.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        
        # start up the toolbar and add it to the sizer
        self.qtool.Realize()
        vboxsizer.Add(self.qtool, 0, wx.EXPAND)        

        
  
        
        for category in self.telemetry_list:
            vboxsizer.Add(sizer_map[category]['siz'], sizer_map[category]['prop'], wx.EXPAND)
            for box_n in range(1, len(self.telemetry_dict[category].keys()) + 1):
                # setting up temporary variable for code shortness
                obj_entry = self.telemetry_dict[category][box_n]
                obj_entry['object'] = TelemetryBox(parent=self,
                                                   obj_dict=obj_entry,
                                                   path=path) 
                sizer_map[category]['siz'].Add(obj_entry['object'], 1, wx.EXPAND)  
        
        
        # we bind events to the buttons
        self.Bind_Events()
        # place the dictionary in the queue that is shared among
        # threads
        self.telemetry_queue.put(self.telemetry_dict)
        
        self.SetSizer(vboxsizer)
        self.SetPosition(WindowSizeAndPosition('Telemetry')['pos'])
        # starting the logging thread
        self.log_t = LoggerClass(path, option)
        self.log_t.start()
        
        self.UpdateOpMode([opmode_map[0]])
        
        self.Show(True)
        self.GUI_commandwindow = GUI_commandwindow.CommandWindow(None, -1, 'Command Window CubeSat Team Politecnico Torino', self.command_queue)
        self.GUI_nextpasses = None
        
    def UpdateNA(self, msg):
        self.NA_flag = True
        
    def UpdateOpMode(self, mode):
        '''
        This function is used to update the operative mode when the relative message is received.
        '''
        if type(mode[0]).__name__=='int':
            self.op_mode=opmode_map[mode[0]]
        else:
            self.op_mode = mode[0]
        self.telemetry_dict['STATUS'][7]['object'].text_control.SetValue(self.op_mode)
        print ('GUITLM: Setting opmode: ' + self.op_mode)
        
    def UpdateLogger(self):
        wx.CallAfter(Publisher.sendMessage, 'activity.txt', msg=['pacchetto', str(self.telemetry_dict['STATUS'][1]['value'])])
        pass
    
    def CheckAdditional(self):
        #check if the satellite rebooted
        if self.telemetry_dict['STATUS'][3]['value'] > self.telemetry_dict['STATUS'][4]['value'] + 100:
            dlg = wx.MessageDialog(self, 'Satellite has rebooted: mission time is lower than ground mission time',
                         'Warning', wx.OK) 
            result = dlg.ShowModal()
            dlg.Destroy()
            

            with open('../data/reboot_num.txt', "r+") as f:
                data = int(f.read())+1
                f.seek(0)
                f.write(str(data))
                f.truncate()
            
            
        
    def UpdateRoutine(self, msg):
        '''
        Update routine: this is the function that will be called when the update
        message is received by this thread
        '''
        self.Update_GUI()
        #self.Check_Limits()
        self.CheckAdditional()
        self.UpdateLogger()
        
    def Update_GUI(self):
        for category in self.telemetry_dict.keys():
            for subcat in self.telemetry_dict[category]:
                try:
                    obj = self.telemetry_dict[category][subcat]['object'].text_control
                    if self.telemetry_dict[category][subcat]['name'] != 'Current Time':
                        if self.telemetry_dict[category][subcat].has_key('NA_flag') and self.NA_flag == True:
                            obj.SetValue('N/A')
                        else:
                            obj.SetValue(str(self.telemetry_dict[category][subcat]['value']))
                            if category == 'STATUS' and subcat == 6 and self.op_mode!=self.telemetry_dict[category][subcat]['value']:
                                wx.CallAfter(Publisher.sendMessage, 'Operative Mode Update',
                                             mode=[self.telemetry_dict[category][subcat]['value']])
                    else:
                        date = strftime("%a, %d %b %Y %H:%M:%S", localtime()).split(' ')
                        newdate = date[4]
                        obj.SetValue(newdate)
                except:
                    print ('Error in key: ' + category + ', ' + str(subcat))
        self.NA_flag = False
#         for category in self.telemetry_dict.keys():
#             t = PrettyTable([cat, 'value'])
#             i = 0
#         
#             for key, val in d[cat].items():
#                 t.add_row([d[cat][key]['name'], d[cat][key]['value']])
#             i+=1;
#             print t
                
    def Check_Limits(self):
        '''
        In this method I check each telemetry value with the limits contained in the dictionary,
        and change the color of the box accordingly.
        '''
        for category in self.telemetry_dict.keys():
            for box_n in range(1, len(self.telemetry_dict[category].keys()) + 1):
                if not self.telemetry_dict[category][box_n].has_key('NA_flag') or self.NA_flag == False:
                    if self.limit_dict.has_key(self.telemetry_dict[category][box_n]['name']) == True:
                        #setting up temporary variables for keeping the code short
                        name = self.telemetry_dict[category][box_n]['name']
                        value = self.telemetry_dict[category][box_n]['value']
                        obj = self.telemetry_dict[category][box_n]['object'].status_panel
                        if self.limit_dict[name]["red_high"] != None and value >= self.limit_dict[name]["red_high"] :
                            obj.SetBackgroundColour ('red')
                        elif  self.limit_dict[name]["yellow_high"] != None and value >= self.limit_dict[name]["yellow_high"]:
                            obj.SetBackgroundColour ('yellow')
                        elif self.limit_dict[name]["red_low"] != None and value <= self.limit_dict[name]["red_low"]:
                            obj.SetBackgroundColour ('red')
                        elif  self.limit_dict[name]["yellow_low"] != None and value <= self.limit_dict[name]["yellow_low"]:
                            obj.SetBackgroundColour ('yellow')
                        else:
                            try:
                                obj.SetBackgroundColour(wx.NullColor)
                            except:
                                pass
        self.NA_flag = False
    
    def ChangeOpMode(self, evt):
        '''
        This function launches a window to change the operative mode manually
        '''
        self.Disable()
        self.opmode_window = wx.Frame(self, -1, 'Set Current OpMode', pos=(0, 0), size=(250, 80))
        choices = []
        for count in range (5):
            choices += [opmode_map[count]]
        op_box = wx.ComboBox(self.opmode_window, -1, pos=(50, 170), choices=choices, style=wx.CB_READONLY)
        print self.op_mode
        op_box.SetSelection(inverted_opmode_map[self.op_mode])
        self.opmode_window.Show()
        self.opmode_window.Bind(wx.EVT_CLOSE, self.OnCloseOpModeWindow)
        
    def OnCloseOpModeWindow(self, evt):
        self.Enable()
        self.opmode_window.Destroy()
        
    def ActivateEmergencyMode(self, evt):
        dlg = wx.MessageDialog(self, 'In this mode the Ground Station will assume every character that has been read is' + 
                         ' a character that has been transmitted by the satellite.\nIt will try to recover' + 
                         ' telemetry information from the characters that are read. Continue?',
                         'Activating Emergency Mode', wx.YES_NO) 
        result = dlg.ShowModal()
        dlg.Destroy()
        if result == wx.ID_NO:
            print 'Emergency Mode not activated.'
        else:
            pass
        
    def OnSelectOpMode(self, event):
        wx.CallAfter(Publisher.sendMessage, 'Operative Mode Update', mode=[opmode_map[event.GetSelection()]])
        self.Enable()
        self.opmode_window.Destroy()
        
    def SetCMDWindowOnTop(self,evt):
        try:
            self.GUI_commandwindow.SetFocus()
        except:
            print 'Error in setting top focus'
    
    def GUI_show_passes(self, evt):
        self.GUI_nextpasses = GUI_passes.MyFrame(None,-1, "Next Passes")
    
    def Bind_Events(self):    
        # bind events to buttons (the id matters)
        '''close window event'''
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.Bind(wx.EVT_SET_FOCUS, self.SetCMDWindowOnTop)
        '''combobox event'''
        self.Bind(wx.EVT_COMBOBOX, self.OnSelectOpMode)        
        '''toolbar events'''
        self.Bind(wx.EVT_TOOL, self.InviaComando, id=98)
        self.Bind(wx.EVT_TOOL, self.clock, id=97) 
        self.Bind(wx.EVT_TOOL, self.date , id=96)
        self.Bind(wx.EVT_TOOL, self.showlogfolder, id=94)
        self.Bind(wx.EVT_TOOL, self.ChangeOpMode, id=93)
        self.Bind(wx.EVT_TOOL, self.GUI_show_passes, id=92)
    
    
    def OnClose(self, evt):
        '''
        Terminate the app and the serial thread once the close button is pressed.
        '''
        self.serial_thread.signal_stop()
        self.log_t.signal_stop()
        try:
            self.initial_window_obj.signal_stop()
        except:
            pass
        try:
            self.GUI_commandwindow.OnClose()
        except:
            pass
        try:
            self.LED_clock.Destroy()
        except:
            pass
        self.Destroy()
        
    def InviaComando(self, evt):
        self.GUI_commandwindow = GUI_commandwindow.CommandWindow(None, -1, 'Command Window E-STAR2 Team Politecnico Torino', self.command_queue)
    def clock(self, evt):
        self.LED_clock = LED_clock(None, -1, WindowSizeAndPosition('Clock')['pos'])
        self.LED_clock.Show()
    def date(self, evt):
        CALENDARIO.start()
    def showlogfolder(self, evt):
        print self.path
        if sys.platform == 'darwin':
            os.system('open -- '+ self.path)
        elif sys.platform == 'linux2':
            os.system('gnome-open '+ self.path)


    def Return_Dim(self, section, name):
        '''
        this class returns the dimensions for the sizers of the telemetry section''' 
        if name == 'telemetry':
            row_n = len(self.telemetry_dict[section].keys()) / MAX_ROW_DIM
            if len(self.telemetry_dict[section].keys()) % MAX_ROW_DIM > 0:
                row_n += 1 
            return row_n
        
        elif name == 'command':
            row_n = len(self.command_dict[section].keys()) / MAX_COL_DIM
            if len(self.command_dict[section].keys()) % MAX_ROW_DIM > 0:
                row_n += 1 
            return row_n
        

class MyOutputWindow(wx.PyOnDemandOutputWindow):
    def __init__(self) :
        wx.PyOnDemandOutputWindow.__init__(self)
        b = 1376
        h = 768
        self.pos = WindowSizeAndPosition('Console', b, h)['pos']
        self.size = WindowSizeAndPosition('Console', b, h)['size']
        self.title = 'Console'


def start(path=None, option='w', mode=None, initial_window_obj=None):
    '''
    This external function (not a method of any class) has been created in order to allow
    execution of this file as a standalone app (with the if check on name) or to call it from
    GUI program, on button press
    '''
    #wx.App.outputWindowClass = MyOutputWindow
    if path == None:
        path = '../log'
    MyGUI(None, -1, 'Ground Control Station interface CubeSat Team', path, option, mode, initial_window_obj)
    

if __name__ == '__main__':
    print 'Cannot run this program as main! Run GUI_main.py'
    wx.App.outputWindowClass = MyOutputWindow
    app = wx.App(redirect=False)
    start()      
