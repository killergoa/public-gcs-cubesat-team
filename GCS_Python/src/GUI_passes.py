import wx
import getpass
from passes import next_passes

proj_name = 'estar2_Simulation'
save_path = '/home/'+getpass.getuser()+'/Dropbox/Repository/ADCS_e-st@r-II/'+ proj_name +'/config/'
save_name = 'setup_' + proj_name + '.ini'
root_folder = '/home/'+getpass.getuser()+'/Dropbox/Repository/ADCS_e-st@r-II/'+ proj_name


class MyFrame(wx.Frame):
    '''
    Frame for the selection of the functions
    '''
    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title, wx.DefaultPosition)
        self.passes_list = None
        self.last_evt = None
        #horizontal sizer to include the (treectrl sizer + buttons gridsizer) and the listctrl sizer(right)
        #later we will assign to hbox the sizers, not widgets
        hbox  = wx.BoxSizer(wx.HORIZONTAL)
        widths=[120,100,100,200,200,200,200,200,200]
        sum=0
        for i in widths:
            sum+=i
        self.SetSize((sum,600))
        #vertical sizer for the listctrl
        vbox2 = wx.BoxSizer(wx.VERTICAL)
        #vertical sizer for the treectrl
        scrollsiz = wx.BoxSizer(wx.VERTICAL)
        
        self.lc = wx.ListCtrl(self, 8, style=wx.LC_REPORT)
        self.lc.InsertColumn(0, 'Date', width=widths[0])
        self.lc.InsertColumn(1, 'Start Time', width=widths[1])
        self.lc.InsertColumn(2, 'Duration', width=widths[2])
        self.lc.InsertColumn(3, 'Start Az', width=widths[3])
        self.lc.InsertColumn(4, 'Max Altitude', width=widths[4])
        self.lc.InsertColumn(5, 'Azimuth of Max', width=widths[5])
        self.lc.InsertColumn(6, 'Elevation at Max', width=widths[6])
        self.lc.InsertColumn(7, 'End Time', width=widths[7])
        self.lc.InsertColumn(8, 'End Azimuth', width=widths[8])
        #self.lc.InsertColumn(8, 'Elevation [km]')
        
        self.Bind_Events()

        vbox2.Add(self.lc, 1, wx.EXPAND | wx.ALL, 3)

        hbox.Add(vbox2, 1, wx.EXPAND)
        self.SetSizer(hbox)
        self.get_next_passes(15)
        count=0
        for item in self.passes_list:
            el = item['start']
            el=str(el).split(' ')
            el2 = item['end']
            el2=str(el2).split(' ')
            self.lc.InsertStringItem(count,el[0])
            self.lc.SetStringItem(count,1,el[1])
            self.lc.SetStringItem(count,7,el2[1])
            t1 = el[1].split(':')
            t2 = el2[1].split(':')
            duration = int(t2[1])-int(t1[1])
            if duration<0:
                duration=duration+60
            self.lc.SetStringItem(count,2,str(duration))
            # to substitute with max
            self.lc.SetStringItem(count,4,str(item['alt'][0]))
            # to substitute with max
            self.lc.SetStringItem(count,3,str(item['azimuth'][0]))
            self.lc.SetStringItem(count,5,str(item['azimuth'][0]))
            self.lc.SetStringItem(count,6,str(item['sublat'][0]))
            self.lc.SetStringItem(count,8,str(item['azimuth'][-1]))
            #self.lc.SetStringItem(count,8,str(item['elevation'][0]))
            
            count+=1
        self.main_while = 0
        self.Show()
        
    def get_next_passes(self, number):
        self.passes_list=next_passes(number, False)
        
    def Bind_Events(self):
        # close window event
        self.Bind(wx.EVT_CLOSE, self.OnClose)
                
    
    def OnClose(self,evt):
        '''
        Terminate the app once the close button is pressed.
        '''
        
        self.Destroy()

        
        
        
        
class MyApp(wx.App):
    '''
    This class is the class for the main window.
    '''
    def OnInit(self):
        frame = MyFrame(None, -1, "Next Passes")
        frame.Show(True)
        self.SetTopWindow(frame)
        return True

def start():
    '''
    This external function (not a method of any class) has been created in order to allow
    execution of this file as a standalone app (with the if check on name) or to call it from
    GUI program, on button press
    '''
    app2 = MyApp(0)
    import wx.lib.inspection
    #wx.lib.inspection.InspectionTool().Show()
    app2.MainLoop()
    

if __name__ == '__main__':
    start()