'''
Created on 31/lug/2014

@author: Lorenzo Feruglio
'''
import struct
from pylab import *
from satellite_mission_params import *
import datetime as dt
from time import localtime

bias_a=(33040, 32891, 32579)
bias_g=(32287, 34048, 33512)
bias_m=(32131, 33024, 32117)
k_ai=((0.000756354016640696, 7.48988771534832e-06, 0.0), (-7.56354016640696e-06, 0.000748988771534832, 0.0), (-1.49758095294858e-05, -1.51295731850036e-05, 0.000754716981132075))
k_gi=((0.000231213872832370, 0.0, 4.545454545454546e-6), (0.0, 0.000231213872832370, 2.754820936639119e-5), (0.0, 0.0, 0.000227272727272727))
k_mi=((1.347992591350662e-4, -2.419527435805742e-6, 9.367002873919932e-6), (-3.576472133639620e-6, 1.357134934447402e-4, 1.054464629882027e-5), (5.843820505154551e-6, -1.517703573369056e-5, 1.353207048129733e-4))
R_coil=(49.6, 48.3, 47.9)

###############################################################################
#                                                                             #
#                            RELEASE TIME                                     #
#                                                                             #
###############################################################################
def releasetime(value_list):
    fp=open('../data/release_time.txt')
    lines=fp.readlines()
    l=lines[1].split(' ')
    a=l[0].split('/')
    b=l[1].split(':')
    for i in range(3):
        a[i]=int(a[i])
        b[i]=int(b[i])
    release = dt.datetime(a[2],a[1],a[0],b[0],b[1],b[2])
    current=localtime()
    current=dt.datetime(current.tm_year, current.tm_mon, current.tm_mday, current.tm_hour, current.tm_min, current.tm_sec)
    
    return int((current-release).total_seconds())

releasetime_dict={'func':releasetime, 'cont':[], 'var_func':None}

###############################################################################
#                                                                             #
#                          SATELLITE TIME                                     #
#                                                                             #
###############################################################################
def satellitetime(value_list):
    
    return (value_list[0]+value_list[1]+value_list[2]+value_list[3])/(100)
def satellitetime_var1(var):
    return int(var)*(256**3)
def satellitetime_var2(var):
    return int(var)*(256**2)
def satellitetime_var3(var):
    return int(var)*(256)

satellitetime_dict={'func':satellitetime, 'cont':[18,19,20,21], 'var_func':[satellitetime_var1,satellitetime_var2,satellitetime_var3,None]}

###############################################################################
#                                                                             #
#                            PACKET N                                         #
#                                                                             #
###############################################################################

def packetn(value_list):
    return value_list[0]+value_list[1]

def packetn_var1(var):
    return int(var)*256 - 1
packetn_dict={'func':packetn, 'cont':[22,23], 'var_func':[packetn_var1, None]}

###############################################################################
#                                                                             #
#                            N REBOOT OBC                                     #
#                                                                             #
###############################################################################
def nrebootobc(value_list):
    fp=None
    try:
        fp=open('../data/reboot_num.txt','r')
    except:
        pass
    if fp==None:
        local_reboot=value_list[0]
        fp=open('../data/reboot_num.txt','w')
        fp.write(str(local_reboot))
        fp.close()
    else:
        local_reboot=fp.readlines()
        local_reboot=int(local_reboot[0])
        print 'local',local_reboot
        fp.close()
    if local_reboot>value_list[0]:
        local_reboot+=1
        fp=open('../data/reboot_num.txt','w')
        fp.write(str(local_reboot))
        fp.close()
    return local_reboot

nrebootobc_dict={'func':nrebootobc, 'cont':[24], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            STATUS ADCS                                      #
#                                                                             #
###############################################################################
def statusadcs(value_list):
    status_map = {0:'OFF',
                  1:'DETUMBLING',
                  2:'DETERMINATION - PAYLOAD'}
    return status_map[value_list[0]]

statusadcs_dict={'func':statusadcs, 'cont':[27], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            OPERATIVE MODE                                   #
#                                                                             #
###############################################################################

def operativemode(value_list):
    return opmode_map[value_list[0]]
                    
operativemode_dict={'func':operativemode, 'cont':[28], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            LAST COMMAND                                     #
#                                                                             #
###############################################################################

def lastcommand(value_list):
    command_map = {0 :'Update mission time',
                   1 :'Reboot',
                   2 :'SMS',
                   3 :'Set TX time',
                   4 :'Download telemetry',
                   5 :'Torquers polarity',
                   6 :'Update orbit parameters',
                   7 :'Stop reburn',
                   8 :'Update UTC',
                   9 :'ADCS mode',
                   10:'Save energy',
                   11:'Set TX preamble',
                   12:'Live again',
                   13:'Stop TX',
                   14:'Resume TX',
                   15:'Stop CW',
                   255:'no command'}
    return command_map[value_list[0]]
                    
lastcommand_dict={'func':lastcommand, 'cont':[29], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            BAT 1 vol                                        #
#                                                                             #
###############################################################################
def bat1vol(value_list):
    return (int(value_list[0])*4*(-0.00951) + 9.773327188)

bat1vol_dict={'func':bat1vol, 'cont':[30], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            BAT 2 vol                                        #
#                                                                             #
###############################################################################
def bat2vol(value_list):
    return int(value_list[0])*4*(-0.00951) + 9.773327188

bat2vol_dict={'func':bat2vol, 'cont':[31], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            (BCR2) SP X V                                    #
#                                                                             #
###############################################################################
def spxvol(value_list):
    return int(value_list[0])*4*(-0.021629698) + 25.13548953

spxvol_dict={'func':spxvol, 'cont':[32], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            (BCR1) SP Y V                                    #
#                                                                             #
###############################################################################
def spyvol(value_list):
    return int(value_list[0])*4*(-0.021318282) + 24.84440402

spyvol_dict={'func':spyvol, 'cont':[33], 'var_func':[None]}

###############################################################################
#                                                                             #
#                             (BCR3) SP Z V                                   #
#                                                                             #
###############################################################################
def spzvol(value_list):
    return int(value_list[0])*4*(-0.021712534) + 25.21372986

spzvol_dict={'func':spzvol, 'cont':[34], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            5V CUR                                           #
#                                                                             #
###############################################################################
def cur5v(value_list):
    return (int(value_list[0])*4* (-7.176197002) + 5703.865062)

cur5v_dict={'func':cur5v, 'cont':[35], 'var_func':[None]}
###############################################################################
#                                                                             #
#                            3.3V CUR                                         #
#                                                                             #
###############################################################################
def cur33v(value_list):
    return (int(value_list[0])*4*(-5.651472599)+4502.486962)

cur33v_dict={'func':cur33v, 'cont':[36], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            BAT 1 CUR                                        #
#                                                                             #
###############################################################################
def bat1cur(value_list):
    return (int(value_list[0])*4*(-3.400150413) + 3078.908564)

bat1cur_dict={'func':bat1cur, 'cont':[37], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            BAT 2 CUR                                        #
#                                                                             #
###############################################################################
def bat2cur(value_list):
    return (int(value_list[0])*4*(-3.400150413) + 3078.908564)

bat2cur_dict={'func':bat2cur, 'cont':[38], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            BAT 1 status                                     #
#                                                                             #
###############################################################################
def bat1status(value_list):
    if value_list[0] <120:
        return 'charge'
    if value_list[0]>120:
        return 'discharge' 
    else:
        return 'nothing!'

bat1status_dict={'func':bat1status, 'cont':[39], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            BAT 2 status                                     #
#                                                                             #
###############################################################################
def bat2status(value_list):
    if value_list[0] <120:
        return 'charge'
    if value_list[0]>120:
        return 'discharge' 
    else:
        return 'nothing!'

bat2status_dict={'func':bat2status, 'cont':[40], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            BAT BUS CUR                                      #
#                                                                             #
###############################################################################
def batbuscur(value_list):
    return (int(value_list[0])*4*(-5.38330814)+4282.822877)

batbuscur_dict={'func':batbuscur, 'cont':[41], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            (BCR2)  SP+X CUR                                 #
#                                                                             #
###############################################################################
def spplusxcur(value_list):
    return int(value_list[0])*4*(-2.820388983) + 2217.040073

spplusxcur_dict={'func':spplusxcur, 'cont':[42], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            (BCR1) SP+Y CUR                                  #
#                                                                             #
###############################################################################
def spplusycur(value_list):
    return int(value_list[0])*4*(-2.732644018) + 2150.468735

spplusycur_dict={'func':spplusycur, 'cont':[43], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            (BCR1) SP-Y CUR                                  #
#                                                                             #
###############################################################################
def spminycur(value_list):
    return int(value_list[0])*4*(-2.654363574) + 2112.140048

spminycur_dict={'func':spminycur, 'cont':[44], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            (BCR3) SP+Z CUR                                  #
#                                                                             #
###############################################################################
def sppluszcur(value_list):
    return int(value_list[0])*4*(-2.524730077) + 2012.780257

sppluszcur_dict={'func':sppluszcur, 'cont':[45], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            (BCR3) SP-Z CUR                                  #
#                                                                             #
###############################################################################
def spminzcur(value_list):
    return int(value_list[0])*4*(-2.538893005) + 2016.431474

spminzcur_dict={'func':spminzcur, 'cont':[46], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            BAT 1 TEMP                                       #
#                                                                             #
###############################################################################
def bat1temp(value_list):
    return int(value_list[0])*4*(-0.3259) + 132.169

bat1temp_dict={'func':bat1temp, 'cont':[47], 'var_func':[None]}



###############################################################################
#                                                                             #
#                            BAT 2 TEMP                                       #
#                                                                             #
###############################################################################
def bat2temp(value_list):
    return int(value_list[0])*4*(-0.3259) + 132.169

bat2temp_dict={'func':bat2temp, 'cont':[48], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            SP+X TEMP                                        #
#                                                                             #
###############################################################################
def spplusxtemp(value_list):
    return int(value_list[0])*4*(-0.148264907135875) + 102.8375

spplusxtemp_dict={'func':spplusxtemp, 'cont':[49], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            SP+Y TEMP                                        #
#                                                                             #
###############################################################################
def spplusytemp(value_list):
    return int(value_list[0])*4*(-0.148264907135875) + 102.8375

spplusytemp_dict={'func':spplusytemp, 'cont':[50], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            SP-Y  TEMP                                       #
#                                                                             #
###############################################################################
def spminytemp(value_list):
    return int(value_list[0])*4*(-0.148264907135875) + 102.8375

spminytemp_dict={'func':spminytemp, 'cont':[51], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            SP+Z  TEMP                                       #
#                                                                             #
###############################################################################
def spplusztemp(value_list):
    return int(value_list[0])*4*(-0.148264907135875) + 102.8375

spplusztemp_dict={'func':spplusztemp, 'cont':[52], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            SP-Z  TEMP                                       #
#                                                                             #
###############################################################################
def spminztemp(value_list):
    return int(value_list[0])*4*(-0.148264907135875) + 102.8375

spminztemp_dict={'func':spminztemp, 'cont':[53], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            S FLAG                                           #
#                                                                             #
###############################################################################
def sflag(value_list):
    return int(value_list[0])

sflag_dict={'func':sflag, 'cont':[54], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            POLARITY                                         #
#                                                                             #
###############################################################################

def polarity(value_list):
    import re
    if value_list[0]<0 or value_list[0]>7:
        return 'error'
    else:
        tmp = bin(int(value_list[0]))[2:].zfill(3)
        tmp = re.sub('[0]', '+', tmp)
        tmp = re.sub('[1]', '-', tmp)
        return tmp[0] + '  ' + tmp[1] + '  ' + tmp[2]

polarity_dict={'func':polarity, 'cont':[55], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            N REBOOT ARM                                     #
#                                                                             #
###############################################################################
def nRebootARM(value_list):
    return int(value_list[0])

nRebootARM_dict={'func':nRebootARM, 'cont':[56], 'var_func':[None]}

###############################################################################
#                                                                             #
#                            q1                                               #
#                                                                             #
###############################################################################

def q1(value_list):
    return struct.unpack('f', chr(value_list[3])+chr(value_list[2])+chr(value_list[1])+chr(value_list[0]))[0]
    
q1_dict={'func':q1, 'cont':[57,58,59,60], 'var_func':[None, None, None, None]}

###############################################################################
#                                                                             #
#                            Q2                                               #
#                                                                             #
###############################################################################

def q2(value_list):
    return struct.unpack('f', chr(value_list[3])+chr(value_list[2])+chr(value_list[1])+chr(value_list[0]))[0]  
q2_dict={'func':q2, 'cont':[61,62,63,64], 'var_func':[None, None, None, None]}

###############################################################################
#                                                                             #
#                            q3                                               #
#                                                                             #
###############################################################################

def q3(value_list):
    return struct.unpack('f', chr(value_list[3])+chr(value_list[2])+chr(value_list[1])+chr(value_list[0]))[0]
    
q3_dict={'func':q3, 'cont':[65,66,67,68], 'var_func':[None, None, None, None]}

###############################################################################
#                                                                             #
#                            Q4                                               #
#                                                                             #
###############################################################################

def q4(value_list):
    return struct.unpack('f', chr(value_list[3])+chr(value_list[2])+chr(value_list[1])+chr(value_list[0]))[0]
    
q4_dict={'func':q4, 'cont':[69,70,71,72], 'var_func':[None, None, None, None]}    

###############################################################################
#                                                                             #
#                            GYR X                                            #
#                                                                             #
###############################################################################


def gyrX(value_list):
    temp_Omegax=int(value_list[0])+int(value_list[1])
    gg1=int(temp_Omegax)-int(bias_g[1])
    temp=0
    for jj in range(3):
        temp+=k_gi[1][jj]*gg1
    return temp;

def gyrX_var1(var):
    return int(var)*256
# we take the 75,76 which is wobm[1] because of rotation from imu to body
# Xbody = Yimu, Ybody = -Ximu
gyrX_dict={'func':gyrX, 'cont':[75,76], 'var_func':[gyrX_var1, None]}

###############################################################################
#                                                                             #
#                            GYR Y                                            #
#                                                                             #
###############################################################################

def gyrY(value_list):
    temp_Omegax=int(value_list[0])+int(value_list[1])
    gg1=int(temp_Omegax)-int(bias_g[0])
    temp=0
    for jj in range(3):
        temp+=k_gi[0][jj]*gg1
    return -temp;

def gyrY_var1(var):
    return int(var)*256
# we take the 75,76 which is wobm[1] because of rotation from imu to body
# Xbody = Yimu, Ybody = -Ximu
gyrY_dict={'func':gyrY, 'cont':[73,74], 'var_func':[gyrY_var1, None, gyrY_var1, None]}


###############################################################################
#                                                                             #
#                            GYR Z                                            #
#                                                                             #
###############################################################################

def gyrZ(value_list):    
    
    temp_Omegaz=int(value_list[0])+int(value_list[1])
    gg3=int(temp_Omegaz)-int(bias_g[2])
    temp=0
    for jj in range(3):
        temp+=k_gi[2][jj]*gg3
    return temp
    
def gyrZ_var1(var):
    return int(var)*256

gyrZ_dict={'func':gyrZ, 'cont':[77,78], 'var_func':[gyrZ_var1,None]}

###############################################################################
#                                                                             #
#                            MeanBdotNorm0                                    #
#                                                                             #
###############################################################################

def MeanBdotNorm0(value_list):
    temp_MBDN = value_list[0]+value_list[1]
    #divide by 100
    temp_MBDN = temp_MBDN/100
    return temp_MBDN

def MeanBdotNorm0_var1(var):
    return int(var)*256
MeanBdotNorm0_dict={'func':MeanBdotNorm0, 'cont':[79,80], 'var_func':[MeanBdotNorm0_var1, None]}

###############################################################################
#                                                                             #
#                            MeanBdotNorm1                                    #
#                                                                             #
###############################################################################
def MeanBdotNorm1(value_list):
    temp_MBDN = value_list[0]+value_list[1]
    #divide by 100
    temp_MBDN = temp_MBDN/100
    return temp_MBDN

def MeanBdotNorm1_var1(var):
    return int(var)*256
MeanBdotNorm1_dict={'func':MeanBdotNorm1, 'cont':[81,82], 'var_func':[MeanBdotNorm1_var1, None]}

###############################################################################
#                                                                             #
#                            MeanBdotNorm2                                    #
#                                                                             #
###############################################################################

def MeanBdotNorm2(value_list):
    temp_MBDN = value_list[0]+value_list[1]
    #divide by 100
    temp_MBDN = temp_MBDN/100
    return temp_MBDN

def MeanBdotNorm2_var1(var):
    return int(var)*256

MeanBdotNorm2_dict={'func':MeanBdotNorm2, 'cont':[83,84], 'var_func':[MeanBdotNorm2_var1, None]}

###############################################################################
#                                                                             #
#                            MAG X                                            #
#                                                                             #
###############################################################################
def magX(value_list):
    temp_magx=int(value_list[0])+int(value_list[1])
    mm1=int(temp_magx)-int(bias_m[1])
    temp=0
    for jj in range(3):
        temp+=k_mi[1][jj]*mm1
        
    return temp
def magX_var1(var):
    return int(var)*256
# we take the 87,88 which is Bmis[1] because of rotation from imu to body
# Xbody = Yimu, Ybody = -Ximu
magX_dict={'func':magX, 'cont':[87,88], 'var_func':[magX_var1, None]}

###############################################################################
#                                                                             #
#                            MAG Y                                            #
#                                                                             #
###############################################################################
def magY(value_list):
    temp_magx=int(value_list[0])+int(value_list[1])
    mm1=int(temp_magx)-int(bias_m[0])
    temp=0
    for jj in range(3):
        temp+=k_mi[0][jj]*mm1
        
    return -temp
def magY_var1(var):
    return int(var)*256
# we take the 85,86 which is Bmis[0] because of rotation from imu to body
# Xbody = Yimu, Ybody = -Ximu
magY_dict={'func':magY, 'cont':[85,86], 'var_func':[magY_var1, None]}


###############################################################################
#                                                                             #
#                            MAG Z                                            #
#                                                                             #
###############################################################################

def magZ(value_list):
    temp_magz=int(value_list[0])+int(value_list[1])
    mm3=int(temp_magz)-int(bias_m[2])
    temp=0
    for jj in range(3):
        temp+=k_mi[2][jj]*mm3
        
    return temp
def magZ_var1(var):
    return int(var)*256
magZ_dict={'func':magZ, 'cont':[89,90], 'var_func':[magZ_var1, None]}


###############################################################################
#                                                                             #
#                            COIL X                                           #
#                                                                             #
###############################################################################

def coilX(value_list):
    adc1=int(value_list[0])+int(value_list[1])
    DC1=(-1.2228) * adc1 + 1226.7
    V_coil1=(0.0103) * DC1 - 5.3196
    return V_coil1/R_coil[0]*1000

def coilX_var1(var):
    return int(var)*256
coilX_dict={'func':coilX, 'cont':[91,92], 'var_func':[coilX_var1, None]}

###############################################################################
#                                                                             #
#                            COIL Y                                           #
#                                                                             #
###############################################################################

def coilY(value_list):
    adc2=int(value_list[0])+int(value_list[1])
    DC2=(-1.194) * adc2 + 1211.2
    V_coil2=(-0.0102) * DC2 + 5.2434
    return V_coil2/R_coil[1]*1000

def coilY_var1(var):
    return int(var)*256
coilY_dict={'func':coilY, 'cont':[93,94], 'var_func':[coilY_var1, None]}

###############################################################################
#                                                                             #
#                            COIL Z                                           #
#                                                                             #
###############################################################################

def coilZ(value_list):
    adc3=int(value_list[0])+int(value_list[1])
    DC3=(-1.1606) * adc3 + 1193
    V_coil3=(-0.0103) * DC3 + 5.2994
    return V_coil3/R_coil[2]*1000

def coilZ_var1(var):
    return int(var)*256
coilZ_dict={'func':coilZ, 'cont':[95,96], 'var_func':[coilZ_var1, None]}

###############################################################################
#                                                                             #
#                            ISTANTANEOUS FIR OUTPUT                          #
#                                                                             #
###############################################################################
def IstantaneousFIRoutput(value_list):
    temp_istfir = value_list[0]+value_list[1]
    #divide by 100
    temp_istfir = temp_istfir/100
    return temp_istfir

def IstantaneousFIRoutput_var1(var):
    return int(var)*256

IstFIRout_dict={'func':IstantaneousFIRoutput, 'cont':[97,98], 'var_func':[IstantaneousFIRoutput_var1, None]}

###############################################################################
#                                                                             #
#                            ACC NORM                                         #
#                                                                             #
###############################################################################
def accNorm(value_list):
    temp_accnorm = value_list[0]+value_list[1]
    #divide by 1000
    accnorm = temp_accnorm/1000.0
    return accnorm

def accNorm_var1(var):
    return int(var)*256

accNorm_dict={'func':accNorm, 'cont':[99,100], 'var_func':[accNorm_var1, None]}

###############################################################################
#                                                                             #
#                            SIGNATURE                                        #
#                                                                             #
###############################################################################
def signature(value_list):
    for count in range(len(correct_sig)):
        if ord(correct_sig[count])!=value_list[count]:
            return 'BAD'
    return 'OK'

signature_dict={'func':signature, 'cont':[101,102,103,104,105,106,107,108,109,110,111], 'var_func':[None]*11}


if __name__ == '__main__':
    pass