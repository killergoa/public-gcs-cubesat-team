from __future__ import print_function
import sys

def warning(*objs):
    print("WARNING: ", *objs, file=sys.stderr)
    
    
def error(*objs):
    print("ERROR: ", *objs, file=sys.stderr)