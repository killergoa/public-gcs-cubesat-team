'''
Created on 22/mag/2014

@author: Lorenzo Feruglio
'''

import wx

def create_command_dict():
    
    #creo un dizionario con le tre categorie, come da immagine
    command_dict = {'colonna1':dict(),'colonna2': dict(), 'colonna3':dict()}
    #dizionario per l'unica riga di bottoni
    button_row_dict={}
    button_row_dict2={}
    
    #per aggiungere un bottone a una colonna, oppure ad un'altra, basta spostarlo
    #tra le voci di questo dizionario
    command_dict['colonna1'][1]='Update Mission Time'        
    command_dict['colonna1'][2]='Update UTC'        
    command_dict['colonna1'][3]='Update Orbit Parameters'     
    command_dict['colonna1'][4]='Update Torquer Polarity'    
    command_dict['colonna1'][5]='SMS'        
                                       
    command_dict['colonna2'][1]='Download Telemetry'        
    command_dict['colonna2'][2]='TX Preamble'        
    command_dict['colonna2'][3]='TX Time'        
    command_dict['colonna2'][4]='ADCS mode'               
    command_dict['colonna2'][5]=button_row_dict                          
                                                                                    
    command_dict['colonna3'][1]='Reboot'              
    command_dict['colonna3'][2]='Stop reburn'
    #command_dict['colonna3'][3]='Save energy'  
    command_dict['colonna3'][3]='Stop CW'             
    command_dict['colonna3'][4]='Resume TX'   
    command_dict['colonna3'][5]='CommandTime'                                        
                                       
    button_row_dict[1]='Save energy'   
    button_row_dict[2]='Live again'    
    button_row_dict[3]='Stop TX'       
    
    command_list=['colonna1','colonna2','colonna3']
                
    return command_dict, command_list, button_row_dict
    
def create_window_items_dict(obj):
    '''
    This is the dictionary that stores all the information of all the single
    entities of the command window.
    Different parameters are available:
    BUTTON NAME: The name on the button
    DESCRIPTION: The label on top of the TextControl
    TYPE: Whether it's a command_box or a button
    OBJECT: Where we store the created object
    FUNCTION: The function to call on the pressing of the button
    NEED_CONFIRM: If True, a popup window must appear to ask for confirmation
    AVAILABLE_IN: List of all the operative modes this command is available in
    REQUIRED_MODE: The operative mode the satellite must be to execute this command
    TRASPONDER: If True, GCS will wait the reception of the transponder
    '''
    window_item_dict = dict()
    window_item_dict['Update Mission Time'      ] = {'button_name':'Update Mission Time',
                                                     'description':'Update mission time [days hours mins secs]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Update_MissionTime,
                                                     'needs confirm':False,
                                                     'required mode':['Basic mission',
                                                                      'Detumbling',
                                                                      'Determination'],
                                                     'expected_input':{'type':'list',
                                                                       'len': 4},
                                                     'trasponder':True}    window_item_dict['Update UTC'               ] = {'button_name':'Update UTC',
                                                     'description':'Update orbit time (UTC) [115 month day hr min sec]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Update_UTC,
                                                     'needs confirm':False,
                                                     'required mode':['Detumbling',
                                                                      'Determination'],
                                                     'expected_input':{'type':'list',
                                                        'len': 6},
                                                     'commands needed':2,
                                                     'trasponder':True}              window_item_dict['Update Orbit Parameters'  ] = {'button_name':'Update Orbit Parameters',
                                                     'description':'Orbital parameters [i raan e w M h 0 0]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'needs confirm':False,
                                                     'function':obj.Update_orbit_parameters,
                                                     'required mode':['Detumbling',
                                                                      'Determination'],
                                                     'expected_input':{'type':'list',
                                                                       'len': 8},
                                                     'commands needed':2,
                                                     'trasponder':True}      
    window_item_dict['Update Torquer Polarity'  ] = {'button_name':'Update Torquer Polarity',
                                                     'description':'Torquers Polarity [+/- +/- +/-]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'needs confirm':True,
                                                     'function':obj.Update_torquers_polarity,
                                                     'required mode':['Detumbling'],
                                                     'expected_input':{'type':'list',
                                                                       'len': 3},
                                                     'trasponder':True}                                                          window_item_dict['SMS'                      ] = {'button_name':'SMS',
                                                     'description':'Send a short message [max 20 characters]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.SMS,
                                                     'needs confirm':False,
                                                     'required mode':['Basic mission',
                                                                      'Detumbling',
                                                                      'Determination'],
                                                     'expected_input':{'type':'str',
                                                                       'len': 20},
                                                     'trasponder':True}                                                                                                                             window_item_dict['Download Telemetry'       ] = {'button_name':'Download Telemetry',
                                                     'description':'Lenght[s] Interval [30 1]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Download_telemetry,
                                                     'needs confirm':False,
                                                     'required mode':['Basic mission',
                                                                      'Detumbling',
                                                                      'Determination'],
                                                     'expected_input':{'type':'list',
                                                                       'len': 2},
                                                     'disable':True,
                                                     'trasponder':True}                                             window_item_dict['TX Preamble'              ] = {'button_name':'TX Preamble',
                                                     'description':'Transmission preamble [170<x<255]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Set_preamble,
                                                     'needs confirm':False,
                                                     'required mode':['Basic mission',
                                                                      'Detumbling',
                                                                      'Determination'],
                                                     'expected_input':{'type':'int',
                                                                       'len': 1},
                                                     'trasponder':True}                                       window_item_dict['TX Time'                  ] = {'button_name':'TX Time',
                                                     'description':'Set TX time [1=30s, 2=60s, ...]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Set_transmission_time,
                                                     'needs confirm':False,
                                                     'required mode':['Basic mission',
                                                                      'Detumbling',
                                                                      'Determination'],
                                                     'expected_input':{'type':'int',
                                                                       'len': 1},
                                                     'trasponder':True}                                            window_item_dict['ADCS mode'                ] = {'button_name':'ADCS mode',
                                                     'description':'ADCS [0=OFF, 1=DETUM/SAFE, 2=DETERM]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Manage_ADCS,
                                                     'needs confirm':False,
                                                     'required mode':['Basic mission',
                                                                      'Detumbling',
                                                                      'Determination'],
                                                     'expected_input':{'type':'int',
                                                                       'len': 1},
                                                     'trasponder':True,
                                                     'sets mode':True}                                          window_item_dict['Save energy'              ] = {'button_name':'Save energy',
                                                     'description':'Save energy',
                                                     'type':'button',
                                                     'object':None,
                                                     'function':obj.Save_Energy,
                                                     'needs confirm':True,
                                                     'function name':'Save_Energy',
                                                     'required mode':['Basic mission',
                                                                      'Detumbling',
                                                                      'Determination'],
                                                     'manual opmode':True,    
                                                     'trasponder':False,
                                                     'sets mode':3}             window_item_dict['Live again'               ] = {'button_name':'Live again',
                                                     'description':'Live again',
                                                     'type':'button',
                                                     'object':None,
                                                     'function':obj.Live_again,
                                                     'function name':'Live_again',
                                                     'needs confirm':False,
                                                     'required mode':['Save energy'],
                                                     'custom message':'You might not receive the transponder.',
                                                     'trasponder':True,
                                                     'sets mode':0}                window_item_dict['Stop TX'                  ] = {'button_name':'Stop TX',
                                                     'description':'Stop TX',
                                                     'type':'button',
                                                     'object':None,
                                                     'function':obj.Stop_TX,
                                                     'function name':'Stop_TX',
                                                     'needs confirm':True,
                                                     'custom message':'You might not receive the transponder.\n CW will still be transmitted if required.',
                                                     'required mode':['Basic mission',
                                                                      'Detumbling',
                                                                      'Determination'],
                                                     'manual opmode':True, 
                                                     'store opmode': True, 
                                                     'trasponder':False,
                                                     'sets mode':4}                                                                                                          window_item_dict['Reboot'                   ] = {'button_name':'Reboot',
                                                     'description':'Reboot',
                                                     'type':'button',
                                                     'object':None,
                                                     'function':obj.Reboot,
                                                     'function name':'Reboot',
                                                     'custom message':'Please allow a couple of minutes for the satellite to reboot.',
                                                     'needs confirm':True,
                                                     'required mode':['Basic mission',
                                                                      'Detumbling',
                                                                      'Determination',
                                                                      'Save energy',
                                                                      'Silent'],
                                                     'trasponder':True,
                                                     'sets mode':0}                                            window_item_dict['Stop reburn'              ] = {'button_name':'Stop reburn',
                                                     'description':'Stop reburn',
                                                     'type':'button','object':None,
                                                     'function':obj.Stop_reburn,
                                                     'function name':'Stop_reburn',
                                                     'needs confirm':False,
                                                     'required mode':['Basic mission',
                                                                      'Detumbling',
                                                                      'Determination'],
                                                     'trasponder':True}                             window_item_dict['Stop CW'                  ] = {'button_name':'Stop CW',
                                                     'description':'Stop CW',
                                                     'type':'button',
                                                     'object':None,
                                                     'function':obj.Stop_CW,
                                                     'function name':'Stop_CW',
                                                     'needs confirm':True,
                                                     'required mode':['Basic mission',
                                                                      'Detumbling',
                                                                      'Determination',
                                                                      'Save energy',
                                                                      'Silent'],
                                                     'trasponder':False}
    window_item_dict['Resume TX'                  ] = {'button_name':'Resume TX',
                                                     'description':'Resume TX',
                                                     'type':'button',
                                                     'object':None,
                                                     'function':obj.Resume_TX,
                                                     'function name':'Resume TX',
                                                     'needs confirm':False,
                                                     'required mode':['Silent'],
                                                     'trasponder':False,
                                                     'manual opmode':True, 
                                                     'sets mode':'previous mode'}
    window_item_dict['CommandTime'                ] = {'button_name':'CommandTime',
                                                     'description':'Time of the last command sent',
                                                     'object':None,
                                                     'type':'infobox'}
    
    return window_item_dict