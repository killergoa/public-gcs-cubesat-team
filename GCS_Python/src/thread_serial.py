'''
Created on 09/giu/2014

@author: Lorenzo Feruglio, Giovanna Rinaldi
'''

import threading
import time
import serial
import platform
from convert import convert
from dict_conversions import correct_sig
from wx.lib.pubsub import setupkwargs
from wx.lib.pubsub import pub as Publisher
from satellite_mission_params import *
import wx
 

DEBUG = False
TEST = True

CYCLE_SLEEP = 1
if TEST: CYCLE_SLEEP = 5
def debug(message):
    if DEBUG:
        print message

serial_port_dict = {'Darwin':'/dev/tty.usbserial-PTWH0PKW',
                    'Linux' :'/dev/ttyUSB'}


class SerialReadingThread(threading.Thread):
    def __init__(self, parent=None, mode=None):
        '''
        @param parent: The gui object that should recieve the value
        @param value: value to 'calculate' to
        '''
        if mode == 'Peer':
            self.peer_mode = True
        else:
            self.peer_mode = False
        self.emergency_mode = False
        self.packet = ''
        self.old_packet = None
        self.parent = parent
        self.telemetry_queue = self.parent.telemetry_queue
        self.command_queue = self.parent.command_queue
        self.telemetry_dict = None
        self.ser = None
        self.sent_command_data='Something different than None! :P'
        self.received_command_data=None
        self.archi = platform.system()
        self.CloseSignal = False
        threading.Thread.__init__(self)
        
    def signal_stop(self):
        '''
        method for stopping the thread
        '''
        self.CloseSignal = True

    def run(self):
        '''Overrides Thread.run. Don't call this directly its called internally
        when you call Thread.start().
        TODO: we need to convert this sleep(1) in a -> serial read (block) and a queue.get (block)
        '''
        if not TEST and not self.peer_mode:
            self.Serial_Setup()
        self.InitialQueueSampling()
        while not self.CloseSignal:
            if self.peer_mode == True or TEST:
                self.ReadPacketFile()
            elif self.command_queue.empty() == True:
                self.ReadSerial()     
            else:
                debug('inside write section')
                command_list=[]
                while not self.command_queue.empty():
                    command_list += [self.command_queue.get(block=False)]
                for count in range(len(command_list)):
                    self.write_serial(command_list[count])
                    #wait 30 seconds before sending second packet
                    if count<len(command_list)-1:
                        time.sleep(30)
            time.sleep(CYCLE_SLEEP)
        print 'Stopping Serial Thread...'
        return None
    
    def InitialQueueSampling(self):
        '''
        Reads the queue initially and grab the dictionary from the Queue
        '''
        self.telemetry_dict=self.telemetry_queue.get(block=True)
        
    def ReadPacketFile(self):
        self.packet = ''
        f = open('packet.txt','r')
        tmp_read = f.read(113)
        f.close()
        if len(tmp_read) == 113:
            self.packet = tmp_read
            convert(False, self.telemetry_dict, self.packet)
            #we send the update message to the GUI: HELOOOO DATA'S READY!!
            wx.CallAfter(Publisher.sendMessage, 'update_NA', msg=None)
            wx.CallAfter(Publisher.sendMessage, 'update', msg=None)
            wx.CallAfter(Publisher.sendMessage, 'conversions.txt', msg=[self.telemetry_dict])
        
    def ReadSerial(self):
        self.packet = ''
        self.packet = self.ser.read(size=5000)
        if len(self.packet)!=0:
            debug(str( len(self.packet)))
        if self.emergency_mode == True:
            #here the function to get the few chars received and to return a packet of 113,
            #can be processed further
            pass
        else:
            if len(self.packet)>=113:
                if self.extract_packet():
                    self.received_command_data=self.packet[22:38]
            elif len(self.packet) < 113 and len(self.packet)!=0:
                #we call this function in case the received packet is split into two
                #different readings
                self.fix_packet()
        if len(self.packet) == 113 and self.sent_command_data!=self.received_command_data:
            #check ADCS data for q
            NA_flag = True
            for ch in self.packet[57:96]:
                if ord(ch) != 113:
                    NA_flag = False
            #send message update N/A
            if NA_flag == True:
                wx.CallAfter(Publisher.sendMessage, 'update_NA', msg=None)
            convert(False, self.telemetry_dict, self.packet)
            time.sleep(1)
            #we send the update message to the GUI: HELOOOO DATA'S READY!!
            wx.CallAfter(Publisher.sendMessage, 'packets.txt', msg=[self.packet])
            wx.CallAfter(Publisher.sendMessage, 'update', msg=None)
            wx.CallAfter(Publisher.sendMessage, 'conversions.txt', msg=[self.telemetry_dict])
        else:
            try:
                if self.packet[18]=='C' and self.packet[19]=='O'and self.packet[20]=='M':
                    wx.CallAfter(Publisher.sendMessage, 'trasponders.txt', msg=[self.packet])
                    wx.CallAfter(Publisher.sendMessage, 'trasponder', msg=[self.packet[22:41]])
            except:
                pass
        
            
    def fix_packet(self):
        tmp = None
        if self.old_packet == None:
            self.old_packet = self.packet
            self.packet = ''
        else:
            tmp = self.old_packet
        if len(self.packet) + len(self.old_packet) == 113:
            self.packet = tmp+self.packet
            self.old_packet = None
            print('Packet has been fixed!')

    def extract_packet(self):
        start_idx = -1
        end_idx = 0
        found = False
        for ch in self.packet:
            if chr(ord(ch))=='\xC0':
                start_idx=self.packet.find('\xC0', start_idx+1)
                if start_idx != -1:
                    found=True
                    count=0
                    for sig_ch in correct_sig:
                        try:
                            if sig_ch != self.packet[start_idx+101+count]:
                                found=False
                                break
                            count+=1
                        except:
                            found=False
                    if found==False:
                        continue
                    end_idx=start_idx+113
                    break
                else:
                    pass
        self.packet=self.packet[start_idx:end_idx]
        return True
        
    def extract_packetV2(self):
        start_idx = self.find_header()
        self.packet = self.find_packet()
        
    def find_header(self):
        found = False
        saved_idx = []
        found_dict = dict()
        #we search the header in the packet, or pieces of it
        for ch in packet_header:
            if not found_dict.has_key(ch):
                    found_dict[ch] = []
            idx = self.packet.find(ch, max(found_dict[ch])+1)
            #we found the first char
            if idx != -1:
                found_dict[ch] += [idx]

        
    def write_serial(self, command):
        print('Sending command: ' + str(len(command)))
        self.sent_command_data=command[20:36]
        if DEBUG:
            for ch in command:
                debug(hex(ord(ch)))
        self.ser.write(command)
    
    def Serial_Setup(self):
        '''
        Serial port setup
        '''
        set_test = False
        if self.archi == 'Darwin':
            try:
                self.ser = serial.Serial(port=serial_port_dict[self.archi],\
                                     baudrate=38400,\
                                     parity=serial.PARITY_NONE,\
                                     stopbits=serial.STOPBITS_ONE,\
                                     bytesize=serial.EIGHTBITS,\
                                     timeout=0)
            except:
                set_test = True
        elif self.archi == 'Linux':
            try:
                self.ser = serial.Serial(port=serial_port_dict[self.archi]+'0',\
                                     baudrate=38400,\
                                     parity=serial.PARITY_NONE,\
                                     stopbits=serial.STOPBITS_ONE,\
                                     bytesize=serial.EIGHTBITS,\
                                     timeout=0)
                print("Opened ttyUSB0\n")
            except:
                try:
                    self.ser = serial.Serial(port=serial_port_dict[self.archi]+'1',\
                                         baudrate=38400,\
                                         parity=serial.PARITY_NONE,\
                                         stopbits=serial.STOPBITS_ONE,\
                                         bytesize=serial.EIGHTBITS,\
                                         timeout=0)
                    print("Opened ttyUSB1\n")
                except:
                    set_test = True
        if set_test == True:
            #if we didn't manage to open any serial, then we are in TEST mode
            print('\n\nWARNING: SETTING TEST MODE\n\n')
            global TEST
            TEST = True
            
        print 'Sending string to start Kiss mode on TNC'
        #emptying the serial port before sending the message to TNC
        if not TEST:
            self.ser.read(size=1000)
            self.ser.write('\x1B\x40\x4B\x0D')
