'''
Created on 14/lug/2014

@author: ws-linux
'''
import serial

if __name__ == '__main__':


    ser = serial.Serial(
        port='/dev/ttyUSB0',\
        baudrate=38400,\
        parity=serial.PARITY_NONE,\
        stopbits=serial.STOPBITS_ONE,\
        bytesize=serial.EIGHTBITS)
    
#     ser2 = serial.Serial(
#         port='/dev/ttyUSB1',\
#         baudrate=9600,\
#         parity=serial.PARITY_NONE,\
#         stopbits=serial.STOPBITS_ONE,\
#         bytesize=serial.EIGHTBITS)
    
    print("connected to: " + ser.portstr)
    count=1
    ser.write('\x0DV\x0D')
    
    while True:
        for line in ser.read():
    
            print(str(count) + str(': ') + (line) )
            count = count+1
    
    ser.close()