'''
Created on 30/giu/2014

@author: ws-linux
'''
from create_telemetry_dict import create_telemetry_dict
import json

if __name__ == '__main__':
    a = create_telemetry_dict()
    a = a[0]
    b=dict()
    for status in a.keys():
        for key in a[status].keys():
            b[a[status][key]['name']] = {'red_low':None, 'yellow_low':None, 'red_high':None,'yellow_high':None}
            
    f = open('starting_dict.txt','w')
    json.dump(b, f, indent=4, sort_keys=True)