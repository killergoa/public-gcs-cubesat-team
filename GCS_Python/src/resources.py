import os
import sys

def resource_path(relative_path, t):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        print('asdasd' + sys._MEIPASS)
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
        if t=='img':
            base_path=os.path.join(base_path,'..','img')
        elif t=='txt':
            base_path=os.path.join(base_path,'..','data')
        elif t=='ico':
            base_path=os.path.join(base_path,'..','img/icons')

    return os.path.join(base_path, relative_path)