'''
Created on 22/mag/2014

@author: Lorenzo Feruglio
'''

import wx

def create_command_dict():
    
    #creo un dizionario con le tre categorie, come da immagine
    command_dict = {'colonna1':dict(),'colonna2': dict(), 'colonna3':dict()}
    #dizionario per l'unica riga di bottoni
    button_row_dict={}
    button_row_dict2={}
    
    #per aggiungere un bottone a una colonna, oppure ad un'altra, basta spostarlo
    #tra le voci di questo dizionario
    command_dict['colonna1'][1]='Update Mission Time'        
    command_dict['colonna1'][2]='Update UTC'        
    command_dict['colonna1'][3]='Update Orbit Parameters'        
    command_dict['colonna1'][4]='Manoeuvre'        
    command_dict['colonna1'][5]='SMS'        
    command_dict['colonna1'][6]='Download Telemetry'                                   
            
    command_dict['colonna2'][1]='TX Preamble'        
    command_dict['colonna2'][2]='TX Time'        
    command_dict['colonna2'][3]='ADCS mode'               
    command_dict['colonna2'][4]='Update GAIN'#button_row_dict
    command_dict['colonna2'][5]=button_row_dict2   
    command_dict['colonna2'][6]=button_row_dict                          
                                                                                    
    command_dict['colonna3'][1]='Reboot'              
    command_dict['colonna3'][2]='Stop reburn'
    command_dict['colonna3'][2]='Save energy'  
    command_dict['colonna3'][3]='Stop CW'             
    #command_dict['colonna3'][4]='Resume TX'                                        
                                       
    button_row_dict[1]='Save energy'   
    button_row_dict[2]='Live again'    
    button_row_dict2[1]='Stop TX'   
    button_row_dict2[2]='Resume TX' 
    #button_row_dict[3]='Stop TX'      
    
    command_list=['colonna1','colonna2','colonna3']
                
    return command_dict, command_list, button_row_dict
    
def create_window_items_dict(obj):
    '''
    This is the dictionary that stores all the information of all the single
    entities of the command window.
    Different parameters are available:
    BUTTON NAME: The name on the button
    DESCRIPTION: The label on top of the TextControl
    TYPE: Whether it's a command_box or a button
    OBJECT: Where we store the created object
    FUNCTION: The function to call on the pressing of the button
    NEED_CONFIRM: If True, a popup window must appear to ask for confirmation
    AVAILABLE_IN: List of all the operative modes this command is available in
    REQUIRED_MODE: The operative mode the satellite must be to execute this command
    TRASPONDER: If True, GCS will wait the reception of the transponder
    '''
    window_item_dict = dict()
    window_item_dict['Update Mission Time'      ] = {'button_name':'Update Mission Time',
                                                     'description':'Update mission time [days hours mins secs]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Update_MissionTime,
                                                     'needs confirm':False,
                                                     'required mode':['Basic Mission',
                                                                      'Full Mission A',
                                                                      'Full Mission B'],
                                                     'expected_input':{'type':'list',
                                                                       'len': 4},
                                                     'trasponder':True}    window_item_dict['Update UTC'               ] = {'button_name':'Update UTC',
                                                     'description':'Update orbit time (UTC) [115 month day hr min sec]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Update_UTC,
                                                     'needs confirm':False,
                                                     'required mode':['Full Mission A',
                                                                      'Full Mission B'],
                                                     'expected_input':{'type':'list',
                                                        'len': 6},
                                                     'commands needed':2,
                                                     'trasponder':True}              window_item_dict['Update Orbit Parameters'  ] = {'button_name':'Update Orbit Parameters',
                                                     'description':'Orbital parameters [i raan e w M nt wc 0 0]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'needs confirm':False,
                                                     'function':obj.Update_orbit_parameters,
                                                     'required mode':['Full Mission A',
                                                                      'Full Mission B'],
                                                     'expected_input':{'type':'list',
                                                                       'len': 8},
                                                     'commands needed':2,
                                                     'trasponder':True}               window_item_dict['Manoeuvre'                ] = {'button_name':'Manoeuvre',
                                                     'description':'q1 q2 q3 q4 [0 0 0 1]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'needs confirm':False,
                                                     'function':obj.Manoeuver,
                                                     'required mode':['Full Mission A',
                                                                      'Full Mission B'],
                                                     'expected_input':{'type':'list',
                                                                       'len': 4},
                                                     'trasponder':True}                                                   window_item_dict['SMS'                      ] = {'button_name':'SMS',
                                                     'description':'Send a short message [max 20 characters]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.SMS,
                                                     'needs confirm':False,
                                                     'required mode':['Basic Mission',
                                                                      'Full Mission A',
                                                                      'Full Mission B'],
                                                     'expected_input':{'type':'str',
                                                                       'len': 20},
                                                     'trasponder':True}                                                                                                                             window_item_dict['Download Telemetry'       ] = {'button_name':'Download Telemetry',
                                                     'description':'Lenght[s] Interval [30 1]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Download_telemetry,
                                                     'needs confirm':False,
                                                     'required mode':['Basic Mission',
                                                                      'Full Mission A',
                                                                      'Full Mission B'],
                                                     'expected_input':{'type':'list',
                                                                       'len': 2},
                                                     'trasponder':True}                                             window_item_dict['TX Preamble'              ] = {'button_name':'TX Preamble',
                                                     'description':'Transmission preamble [1<x<255]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Set_preamble,
                                                     'needs confirm':False,
                                                     'required mode':['Basic Mission',
                                                                      'Full Mission A',
                                                                      'Full Mission B'],
                                                     'expected_input':{'type':'int',
                                                                       'len': 1},
                                                     'trasponder':True}                                       window_item_dict['TX Time'                  ] = {'button_name':'TX Time',
                                                     'description':'Set TX time [1=30s, 2=60s, ...]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Set_transmission_time,
                                                     'needs confirm':False,
                                                     'required mode':['Basic Mission',
                                                                      'Full Mission A',
                                                                      'Full Mission B'],
                                                     'expected_input':{'type':'int',
                                                                       'len': 1},
                                                     'trasponder':True}                                            window_item_dict['ADCS mode'                ] = {'button_name':'ADCS mode',
                                                     'description':'ADCS [0=OFF, 1=ARM+IMU,2=ARM+IMU+MT]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Manage_ADCS,
                                                     'needs confirm':False,
                                                     'required mode':['Basic Mission',
                                                                      'Full Mission A',
                                                                      'Full Mission B'],
                                                     'expected_input':{'type':'int',
                                                                       'len': 1},
                                                     'trasponder':True}      
    window_item_dict['Update GAIN'              ] = {'button_name':'Update GAIN',
                                                     'description':'Gains [gain_X gain_Y gain_Z]',
                                                     'type':'command_box',
                                                     'object':None,
                                                     'function':obj.Update_Gain,
                                                     'needs confirm':True,
                                                     'required mode':['Full Mission A',
                                                                      'Full Mission B'],
                                                     'expected_input':{'type':'list',
                                                                       'len': 3},
                                                     'trasponder':True}                                window_item_dict['Save energy'              ] = {'button_name':'Save energy',
                                                     'description':'Save Energy',
                                                     'type':'button',
                                                     'object':None,
                                                     'function':obj.Save_Energy,
                                                     'needs confirm':True,
                                                     'function name':'Save_Energy',
                                                     'required mode':['Basic Mission',
                                                                      'Full Mission A',
                                                                      'Full Mission B'],
                                                     'manual opmode':True,    
                                                     'trasponder':False}             window_item_dict['Live again'               ] = {'button_name':'Live again',
                                                     'description':'Live again',
                                                     'type':'button',
                                                     'object':None,
                                                     'function':obj.Live_again,
                                                     'function name':'Live_again',
                                                     'needs confirm':False,
                                                     'required mode':['Save Energy'],
                                                     'trasponder':True}                window_item_dict['Stop TX'                  ] = {'button_name':'Stop TX',
                                                     'description':'Stop TX',
                                                     'type':'button',
                                                     'object':None,
                                                     'function':obj.Stop_TX,
                                                     'function name':'Stop_TX',
                                                     'needs confirm':True,
                                                     'custom message':'You might not receive the transponder.\n CW will still be transmitted if required.',
                                                     'required mode':['Basic Mission',
                                                                      'Full Mission A',
                                                                      'Full Mission B'],
                                                     'manual opmode':True,  
                                                     'trasponder':False}                                                                                                          window_item_dict['Reboot'                   ] = {'button_name':'Reboot',
                                                     'description':'Reboot',
                                                     'type':'button',
                                                     'object':None,
                                                     'function':obj.Reboot,
                                                     'function name':'Reboot',
                                                     'custom message':'Please allow a couple of minutes for the satellite to reboot.',
                                                     'needs confirm':True,
                                                     'required mode':['Basic Mission',
                                                                      'Full Mission A',
                                                                      'Full Mission B',
                                                                      'Save Energy',
                                                                      'Silent'],
                                                     'trasponder':True}                                            window_item_dict['Stop reburn'              ] = {'button_name':'Stop reburn',
                                                     'description':'Stop reburn',
                                                     'type':'button','object':None,
                                                     'function':obj.Stop_reburn,
                                                     'function name':'Stop_reburn',
                                                     'needs confirm':False,
                                                     'required mode':['Basic Mission',
                                                                      'Full Mission A',
                                                                      'Full Mission B'],
                                                     'trasponder':True}                             window_item_dict['Stop CW'                  ] = {'button_name':'Stop CW',
                                                     'description':'Stop CW',
                                                     'type':'button',
                                                     'object':None,
                                                     'function':obj.Stop_CW,
                                                     'function name':'Stop_CW',
                                                     'needs confirm':True,
                                                     'required mode':['Basic Mission',
                                                                      'Full Mission A',
                                                                      'Full Mission B',
                                                                      'Save Energy',
                                                                      'Silent'],
                                                     'trasponder':False}
    window_item_dict['Resume TX'                  ] = {'button_name':'Resume TX',
                                                     'description':'Resume TX',
                                                     'type':'button',
                                                     'object':None,
                                                     'function':obj.Resume_TX,
                                                     'function name':'Resume TX',
                                                     'needs confirm':False,
                                                     'required mode':['Silent'],
                                                     'trasponder':False}
    
    return window_item_dict
