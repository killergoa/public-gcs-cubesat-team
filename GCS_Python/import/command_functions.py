'''
Created on 08/lug/2014

@author: Giovanna Rinaldi
'''
# in questo file creo il dizionario con i pacchetti per ogni comando che lancero

import wx
import time    

def create_command_dict_function():
    
    command_dict_function= dict()
    
    
    command_dict_function['Update Mission Time']=          {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x30\x41',
                                                            'value':None,
                                                            'closer':'\xC0'}
    command_dict_function['Update Orbit Parameters']={'A': {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x36\x47', 
                                                            'value':None,
                                                            'closer':'\xC0'},
                                                      'B': {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4,\x92\x84\xA4\x82\x61\x03\xF0\x36\x47', 
                                                            'value':None,
                                                            'closer':'\xC0'}}                    
    command_dict_function['SMS']=                          {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x32\x43', 
                                                            'value':None,
                                                            'closer':'\xC0'}
    command_dict_function['Download Telemetry']=           {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x34\x45', 
                                                            'value':None,
                                                            'closer':'\xC0'}
    command_dict_function['Stop reburn']=                  {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x37\x48', 
                                                            'value':None,
                                                            'closer':'\xC0'}
    command_dict_function['Manoeuvre']=                    {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x35\x46',
                                                            'value':None,
                                                            'closer':'\xC0'} 
    command_dict_function['Reboot']=                       {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x31\x42',
                                                            'value':None,
                                                            'closer':'\xC0'}
    command_dict_function['TX Time']=                      {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x33\x44',
                                                            'value':None,
                                                            'closer':'\xC0'}
    command_dict_function['Update UTC']=             {'A': {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x38\x49,\x47\x4F', 
                                                            'value':None,
                                                            'closer':'\xC0'},
                                                      'B': {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x38\x49', 
                                                            'value':None,
                                                            'closer':'\xC0'}}
    command_dict_function['ADCS mode']=              {'A': {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x39\x4A', 
                                                            'value':None,
                                                            'closer':'\xC0'},
                                                      'B': {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x39\x4A\x49\x4D\x55', 
                                                            'value':None,
                                                            'closer':'\xC0'},
                                                      'C': {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x39\x4A\x47\x4F',
                                                            'value':None,
                                                            'closer':'\xC0'}}
    command_dict_function['Update GAIN']=                  {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x64\x4E',
                                                            'value':None,
                                                            'closer':'\xC0'}
    command_dict_function['Save energy']= {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x61\x4B', 
                                    'value':None,
                                    'closer':'\xC0'}
    command_dict_function['Live again']= {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x61\x4B\x47\x4F', 
                                    'value':None,
                                    'closer':'\xC0'}
    command_dict_function['TX Preamble']= {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x62\x4C', 
                                    'value':None,
                                    'closer':'\xC0'}
    command_dict_function['Stop TX']= {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x63\x4D',
                                    'value':None,
                                    'closer':'\xC0'}
    command_dict_function['Resume TX']= {'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x63\x4D\x47\x4F', 
                                    'value':None,
                                    'closer':'\xC0'}
                                             
    command_dict_function['Stop CW']={'header':'\xC0\x00\x8A\xA6\xA8\x82\xA4\x40\x60\x82\xA4\x92\x84\xA4\x82\x61\x03\xF0\x64\x4E\x2A\x95\x4A\xA5\x52\xA9\x54\xAA\x55\x2A\x95\x4A\xA5',
                                    'value':None,
                                    'closer':'\xC0'}   
    

    
    return command_dict_function

